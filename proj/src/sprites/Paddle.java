package sprites;

import java.awt.Color;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import game.GameLevel;
import geometry.Point;
import geometry.Rectangle;

/**
 * class of paddle. has move functions. implements Sprite and Collidable.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Paddle implements Sprite, Collidable {
    // members
    private biuoop.KeyboardSensor keyboard;
    private Color color;
    private Point upperLeft;
    private Rectangle rect;
    private int width, height;
    private double speed;
    private int boundSize;
    private int guiWidth;

    /**
     * The constructor of the paddle.
     *
     * @param keyboard
     *            a KeyboardSensor object.
     * @param boundsSize
     *            the size of the blocks in the limits of the GUI.
     * @param guiWidth
     *            the width of the GUI.
     * @param guiHeight
     *            the height of the GUI.
     * @param width
     *            the width of the paddle
     * @param speed
     *            the speed of the paddle
     */
    public Paddle(KeyboardSensor keyboard, int boundsSize, int guiWidth, int guiHeight, int width, int speed) {
        // set members
        this.keyboard = keyboard;
        this.color = Color.YELLOW;
        this.width = width;
        this.height = 10;
        this.speed = speed;
        this.boundSize = boundsSize;
        this.guiWidth = guiWidth;
        // the upperLeft is located so the paddle will be in the middle of the
        // bottom of the surface
        this.upperLeft = new Point(guiWidth / 2 - this.width / 2, guiHeight - boundsSize - this.height);
        this.rect = new Rectangle(this.upperLeft, this.width, this.height);
    }

    /**
     * move the paddle right one time if the paddle is not in the limit of the
     * GUI.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void moveLeft(double dt) {
        if (keyboard.isPressed(KeyboardSensor.LEFT_KEY)) {
            // try moving the paddle only if it is inside the boundaries
            if (this.rect.getUpperLeft().getX() > this.boundSize) {
                // the x coordinate where the start of paddle will be after the
                // next move
                int outOfBoundPosition = (int) (this.rect.getUpperLeft().getX() - this.speed * dt);
                // if the paddle will be out of the left boundary after the next
                // move
                if (outOfBoundPosition < this.boundSize) {
                    // set the upperLeft as the last coordinates before the left
                    // boundary
                    this.upperLeft = new Point(this.upperLeft.getX() - (this.speed * dt - 25 + outOfBoundPosition - 1),
                            this.upperLeft.getY());
                } else {
                    // set the upperLeft as the next move by the regular speed
                    this.upperLeft = new Point(this.upperLeft.getX() - this.speed * dt, this.upperLeft.getY());
                }
                // set a new rectangle with the new upperLeft
                this.rect = new Rectangle(this.upperLeft, this.width, this.height);
            }
        }
    }

    /**
     * move the paddle left one time if the paddle is not in the limit of the
     * GUI.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void moveRight(double dt) {
        if (keyboard.isPressed(KeyboardSensor.RIGHT_KEY)) {
            int rightBound = this.guiWidth - this.boundSize;
            // try moving the paddle only if it is inside the boundaries
            if (this.rect.getUpperLeft().getX() < rightBound) {
                // the x coordinate where the end of paddle will be after the
                // next move
                int outOfBoundPosition = (int) (this.rect.getUpperLeft().getX() + this.width + this.speed * dt);
                // if the paddle will be out of the right boundary after the
                // next move
                if (outOfBoundPosition > rightBound) {
                    // set the upperLeft as the last coordinates before the
                    // right boundary
                    this.upperLeft = new Point(
                            this.upperLeft.getX() + (this.speed * dt + rightBound - outOfBoundPosition),
                            this.upperLeft.getY());
                } else {
                    // set the upperLeft as the next move by the regular speed
                    this.upperLeft = new Point(this.upperLeft.getX() + this.speed * dt, this.upperLeft.getY());
                }
                // set a new rectangle with the new upperLeft
                this.rect = new Rectangle(this.upperLeft, this.width, this.height);
            }
        }
    }

    /**
     * call moveLeft and moveRight.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void timePassed(double dt) {
        moveLeft(dt);
        moveRight(dt);
    }

    /**
     * draw the paddle.
     *
     * @param d
     *            a DrawSurface object
     */
    public void drawOn(DrawSurface d) {
        // draw the paddle
        d.setColor(this.color);
        d.fillRectangle((int) upperLeft.getX(), (int) upperLeft.getY(), this.width, this.height);
    }

    /**
     * @return the rectangle of the block.
     */
    public Rectangle getCollisionRectangle() {
        return this.rect;
    }

    /**
     * calculate the new velocity of the ball after hitting the paddle.
     *
     * @param hitter
     *            the ball that hit the block
     * @param collisionPoint
     *            the point of the collision.
     * @param currentVelocity
     *            the velocity of the ball.
     * @return the new velocity.
     */
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        double velDX = currentVelocity.getDX();
        double velDY = currentVelocity.getDY();
        // getting the lineText contains string of the lines that the
        // collision point in them
        String lineText = this.rect.whichLine(collisionPoint);
        // the up side of the paddle is divided to 5 parts, every part set
        // another angle for the velocity. the third one is reacting like the
        // blocks
        if (lineText.contains("UP")) {
            int region = (int) ((collisionPoint.getX() - this.upperLeft.getX()) / (this.width / 5));
            switch (region) {
            case 0:
                return Velocity.fromAngleAndSpeed(300, currentVelocity.calculateSpeed());
            case 1:
                return Velocity.fromAngleAndSpeed(330, currentVelocity.calculateSpeed());
            case 2:
                velDY = -velDY;
                return new Velocity(velDX, velDY);
            case 3:
                return Velocity.fromAngleAndSpeed(30, currentVelocity.calculateSpeed());
            // these 2 last cases are the same part of the paddle. the last one
            // is the end point of the line
            case 4:
                return Velocity.fromAngleAndSpeed(60, currentVelocity.calculateSpeed());
            case 5:
                return Velocity.fromAngleAndSpeed(60, currentVelocity.calculateSpeed());
            default:
                return null;
            }
        }
        if (lineText.contains("DOWN")) {
            velDY = Math.abs(velDY);
        }
        if (lineText.contains("RIGHT")) {
            velDX = Math.abs(velDX);
        }
        if (lineText.contains("LEFT")) {
            velDX = -velDX;
        }
        return new Velocity(velDX, velDY);
    }

    /**
     * add the paddle to the game.
     *
     * @param g
     *            a Game object
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
        g.addCollidable(this);
    }

    /**
     * remove the paddle from the game.
     *
     * @param game
     *            a GameLevel
     */
    public void removeFromGame(GameLevel game) {
        game.removeCollidable(this);
        game.removeSprite(this);
    }
}