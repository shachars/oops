package game;

import java.awt.Color;

import biuoop.DrawSurface;
import sprites.Sprite;

/**
 * class of LivesIndicator. contains constructor, query and command functions.
 * in charge of showing the lives left. implements sprite.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class LivesIndicator implements Sprite {
    // members
    private Counter livesCounter;

    /**
     * constructor of LivesIndicator.
     *
     * @param livesCounter
     *            Counter object that counts the lives.
     */
    public LivesIndicator(Counter livesCounter) {
        this.livesCounter = livesCounter;
    }

    /**
     * draw the lives that left.
     *
     * @param d
     *            a DrawSurface object
     */
    public void drawOn(DrawSurface d) {
        d.setColor(Color.BLACK);
        d.drawText(175, 19, "lives: " + livesCounter.getValue(), 18);
    }

    /**
     * notify the sprite that time has passed.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void timePassed(double dt) {
        return;
    }

    /**
     * add the indicator to the game.
     *
     * @param g
     *            GameLevel object
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
