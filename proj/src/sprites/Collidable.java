package sprites;

import geometry.Point;
import geometry.Rectangle;

/**
 * interface of Collidable. contains the functions getCollisionRectangle and
 * hit.
 *
 * @author Shachar & Yehoshua.
 *
 */
public interface Collidable {
    /**
     * @return the "collision shape" of the object.
     */
    Rectangle getCollisionRectangle();

    /**
     * calculate the new velocity of the ball after hitting the block.
     *
     * @param hitter
     *            the ball that hit the block
     * @param collisionPoint
     *            the point of the collision.
     * @param currentVelocity
     *            the ball's current velocity.
     * @return the update velocity.
     */
    Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity);
}