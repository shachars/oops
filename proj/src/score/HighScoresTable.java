package score;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class of HighScoresTable. Serializable class. contains the best specific
 * number of high scores. can load and save a table using files.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class HighScoresTable implements Serializable {
    // members
    private static final long serialVersionUID = -7845608530587193595L;
    private int size;
    private List<ScoreInfo> scoreList;
    private ObjectInputStream inRead;
    private ObjectOutputStream outWrite;

    /**
     * Create an empty high-scores table with the specified size.
     *
     * @param size
     *            the number of scores that the table can hold when it's full
     */
    public HighScoresTable(int size) {
        this.size = size;
        this.scoreList = new ArrayList<ScoreInfo>();
    }

    /**
     * Add a high-score to the table.
     *
     * @param score
     *            a ScoreInfo object
     */
    public void add(ScoreInfo score) {
        int rank = this.getRank(score.getScore());
        if (rank < this.size) {
            this.scoreList.add(rank - 1, score);
            if (this.scoreList.size() > this.size) {
                this.scoreList.remove(this.scoreList.size() - 1);
            }
        }
    }

    /**
     * @return the table size.
     */
    public int size() {
        return this.size;
    }

    /**
     * @return the current high scores.
     */
    public List<ScoreInfo> getHighScores() {
        return this.scoreList;
    }

    /**
     * return the rank of the current score related the current table.
     *
     * @param score
     *            a score
     * @return the rank of the score.
     */
    public int getRank(int score) {
        int i = 1;
        for (ScoreInfo scoreInfo : scoreList) {
            if (score >= scoreInfo.getScore()) {
                return i;
            }
            i++;
        }
        return i;
    }

    /**
     * Clears the table.
     */
    public void clear() {
        this.scoreList.clear();
    }

    /**
     * Load table data from file. clear the current table data.
     *
     * @param filename
     *            path to file contains a HighScoresTable data
     * @throws IOException
     *             if there is problem in opening the file.
     */
    public void load(File filename) throws IOException {
        try {
            this.clear();
            this.inRead = new ObjectInputStream(new FileInputStream(filename));
            ScoreInfo tempScore = (ScoreInfo) inRead.readObject();
            while (tempScore != null) {
                this.scoreList.add(tempScore);
                tempScore = (ScoreInfo) inRead.readObject();
            }
        } catch (ClassNotFoundException e) {
            System.out.println("Class Not Found in loading file!");
        } catch (IOException e) {
            System.out.println("Something went wrong while loading!");
        } finally {
            if (this.inRead != null) {
                try {
                    this.inRead.close();
                } catch (IOException e) {
                    System.out.println("Failed closing the file !");
                }
            }
        }
    }

    /**
     * Save table data to the specified file.
     *
     * @param filename
     *            path to file
     */
    public void save(File filename) {
        try {
            this.outWrite = new ObjectOutputStream(new FileOutputStream(filename));
            for (Iterator<ScoreInfo> iterator = scoreList.iterator(); iterator.hasNext();) {
                ScoreInfo scoreInfo = (ScoreInfo) iterator.next();
                outWrite.writeObject(scoreInfo);
            }
            outWrite.writeObject(null);
        } catch (IOException e) {
            System.out.println("Something went wrong while saving!");
        } finally {
            if (this.inRead != null) {
                try {
                    this.inRead.close();
                } catch (IOException e) {
                    System.out.println(" Failed closing the file !");
                }
            }
        }
    }

    /**
     * Read a table from file and return it. If the file does not exist, or
     * there is a problem with reading it, an empty table is returned.
     *
     * @param filename
     *            path to file contains a HighScoresTable data
     * @return the table from the file. If the file does not exist, or there is
     *         a problem with reading it,return an empty table
     */
    //
    public static HighScoresTable loadFromFile(File filename) {
        HighScoresTable tempHighScoresTbl = new HighScoresTable(8);
        if (!filename.exists()) {
            return tempHighScoresTbl;
        }
        ObjectInputStream inTempRead = null;
        try {
            inTempRead = new ObjectInputStream(new FileInputStream(filename));
            tempHighScoresTbl.load(filename);
            return tempHighScoresTbl;
        } catch (IOException e) {
            System.out.println("Something went wrong while reading!");
            return tempHighScoresTbl;
        } finally {
            if (inTempRead != null) {
                try {
                    inTempRead.close();
                } catch (IOException e) {
                    System.out.println(" Failed closing the file !");
                }
            }
        }
    }
}