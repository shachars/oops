package animation;

import biuoop.DrawSurface;
import game.Counter;

/**
 * class of EndScreen. shows the end screen. implements Animation.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class EndScreen implements Animation {

    // members
    private Counter livesCounter;
    private Counter scoreCounter;
    private boolean stop;

    /**
     * constructor of EndScreen.
     *
     * @param livesCounter
     *            Counter object, counts lives
     * @param scoreCounter
     *            Counter object, counts score.
     */
    public EndScreen(Counter livesCounter, Counter scoreCounter) {
        this.livesCounter = livesCounter;
        this.scoreCounter = scoreCounter;
        this.stop = false;
    }

    /**
     * show one frame of the end screen.
     *
     * @param d
     *            a DrawSurface object
     * @param dt
     *            seconds passed since last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        // if the player lost
        if (livesCounter.isZero()) {
            d.drawText(10, d.getHeight() / 2, "Game Over. Your score is " + scoreCounter.getValue(), 32);
        } else { // if the player won
            d.drawText(10, d.getHeight() / 2, "You Win! Your score is " + scoreCounter.getValue(), 32);
        }
        d.drawText(150, 550, "prese space to continue", 28);
    }

    /**
     * @return running situation
     */
    public boolean shouldStop() {
        return this.stop;
    }

}
