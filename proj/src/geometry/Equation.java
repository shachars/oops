package geometry;
/**
 * class of Equation, contains constructor, query and command functions.
 *
 * @author Shachar & Yehoshua.
 */
public class Equation {
    // members
    private double slope;
    private double yIntercept;
    private double xConst;
    // check whether the equation has const x
    private boolean constEquation;

    /**
     * constructor of Equation. calculates the equation of the line.
     *
     * @param ln
     *            a line.
     */
    public Equation(Line ln) {
        // calculates the slope and y intercept of the equation
        // using try and catch for the case of equation with const x
        this.slope = Equation.calculateSlope(ln.start(), ln.end());
        this.yIntercept = this.calculateYIntercept(ln.start());
        // the equation does not have const x
        this.constEquation = false;
        // the line is vertical
        if (Double.isInfinite(slope)) {
            this.xConst = ln.start().getX();
            // the equation have const x
            this.constEquation = true;
        }
    }

    /**
     * calculates the slope of the equation.
     *
     * @param a
     *            first point
     * @param b
     *            second point
     * @return the slope of the equation.
     */
    public static double calculateSlope(Point a, Point b) {
        // calculates the slope
        return (a.getY() - b.getY()) / (a.getX() - b.getX());
    }

    /**
     * calculate the y-intercept of the equation.
     *
     * @param a
     *            one point
     * @return the y-intercept of the equation
     */
    public double calculateYIntercept(Point a) {
        // calculate the y intercept
        return a.getY() - this.slope * a.getX();
    }

    /**
     * calculate the y value of one x coordinate.
     *
     * @param x
     *            x coordinate
     * @return the y value of one x coordinate
     */
    public double calculateYValue(double x) {
        // calculate the y value according to the equation
        double yValue = this.slope * x + this.yIntercept;
        return yValue;
    }

    /**
     *
     * @return the slope of the equation.
     */
    public double getSlope() {
        return this.slope;
    }

    /**
     *
     * @return the y-intercept of the equation.
     */
    public double getYIntercept() {
        return this.yIntercept;
    }

    /**
     *
     * @return the constant X of the equation, if the equation has constant X
     *         value.
     */
    public double getXConst() {
        return this.xConst;
    }

    /**
     * check whether the equation has constant x value.
     *
     * @return whether the equation has constant x value.
     */
    public boolean isConstEquation() {
        return this.constEquation;
    }

    /**
     * check whether the point is in the equation's line.
     *
     * @param point
     *            a point.
     * @return true if the point in the equation's line, else false.
     */
    public boolean isOnEquation(Point point) {
        // check whether the point's y is equal to the yValue of the point's
        // x according to this equation
        double epsilon = 0.000000001;
        if (!this.constEquation) {
            return Math.abs(calculateYValue(point.getX()) - point.getY()) < epsilon;
            // if the equation is const equation, checking whether the xConst is
            // equal to the point's x
        } else {
            return this.xConst == point.getX();
        }
    }
}
