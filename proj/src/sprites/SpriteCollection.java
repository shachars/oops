package sprites;

import java.util.ArrayList;

import biuoop.DrawSurface;

/**
 * class of SpriteCollection. contains all the Sprites in the game. can start
 * all the sprites' drawOn and timePassed functions
 *
 * @author Shachar & Yehoshua.
 *
 */
public class SpriteCollection {
    // members
    private ArrayList spriteCollction;

    /**
     * auto constructor. create an arrayList.
     *
     */
    public SpriteCollection() {
        // create empty list
        this.spriteCollction = new ArrayList();
    }

    /**
     * add a sprite.
     *
     * @param s
     *            a sprite.
     */
    public void addSprite(Sprite s) {
        this.spriteCollction.add(s);
    }

    /**
     * remove a Sprite from the spriteCollction.
     *
     * @param s
     *            a Sprite
     */
    public void removeSprite(Sprite s) {
        this.spriteCollction.remove(s);
    }

    /**
     * call timePassed function on all sprites.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void notifyAllTimePassed(double dt) {
        // notify all the sprite that the time passed
        for (int i = 0; i < this.spriteCollction.size(); i++) {
            ((Sprite) this.spriteCollction.get(i)).timePassed(dt);
        }
    }

    /**
     * call drawOn function on all sprites.
     *
     * @param d
     *            a DrawSurface object
     */
    public void drawAllOn(DrawSurface d) {
        // draw all the sprites on
        for (int i = 0; i < this.spriteCollction.size(); i++) {
            ((Sprite) this.spriteCollction.get(i)).drawOn(d);
        }
    }

    /**
     * return a sprite from index i in the spriteCollction.
     *
     * @param i
     *            index
     * @return the sprite in index i in the spriteCollction.
     */
    public Sprite getSprite(int i) {
        return (Sprite) this.spriteCollction.get(i);
    }

    /**
     * @return the spriteCollction list.
     */
    public ArrayList getSpritesList() {
        return this.spriteCollction;
    }
}
