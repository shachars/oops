package animation;

import java.awt.Color;

import biuoop.DrawSurface;
import biuoop.Sleeper;
import sprites.SpriteCollection;

/**
 * class of paddle. counts before the game starts. implements Animation.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class CountdownAnimation implements Animation {
    private double numOfSeconds;
    private int countFrom;
    private SpriteCollection gameScreen;
    private int currentNum;
    private boolean running;

    /**
     * constructor of CountdownAnimation.
     *
     * @param numOfSeconds
     *            the number of seconds
     * @param countFrom
     *            the number to start count from
     * @param gameScreen
     *            collection of the sprites on the game screen
     */
    public CountdownAnimation(double numOfSeconds, int countFrom, SpriteCollection gameScreen) {
        this.numOfSeconds = numOfSeconds;
        this.countFrom = countFrom;
        this.currentNum = countFrom;
        this.gameScreen = gameScreen;
        this.running = true;
    }

    /**
     * show one frame of the countdown according to numOfSeconds/countFrom.
     *
     * @param d
     *            a DrawSurface object
     * @param dt
     *            seconds passed since last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        biuoop.Sleeper sleeper = new Sleeper();
        long startTime = System.currentTimeMillis(); // timing
        gameScreen.drawAllOn(d);
        d.setColor(Color.BLACK);
        if (this.currentNum > -1) {
            if (this.currentNum > 0) {
                d.drawText(d.getWidth() / 2, d.getHeight() / 2, Integer.toString(this.currentNum), 52);
            } else {
                d.drawText(d.getWidth() / 2, d.getHeight() / 2, "GO", 52);
            }
        } else {
            this.running = false;
        }
        long usedTime = System.currentTimeMillis() - startTime;
        long milliSecondLeftToSleep = (long) (this.numOfSeconds / (this.countFrom + 1) - usedTime);
        if (milliSecondLeftToSleep > 0) {
            sleeper.sleepFor(milliSecondLeftToSleep);
        }
        this.currentNum--;
    }

    /**
     * @return running situation
     */
    public boolean shouldStop() {
        return !this.running;
    }
}
