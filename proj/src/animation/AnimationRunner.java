package animation;

import biuoop.DrawSurface;
import biuoop.GUI;

/**
 * class of Animation runner. the class runs Animation implemented objects,
 * contains the gui.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class AnimationRunner {
    // members
    private GUI gui;
    private biuoop.Sleeper sleeper;
    private double dt;

    /**
     * constructor of AnimationRunner.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public AnimationRunner(double dt) {
        this.gui = new GUI("Bouncing Ball Game", 800, 600);
        this.dt = dt;
        this.sleeper = new biuoop.Sleeper();
    }

    /**
     * run the Animation implemented object.
     *
     * @param animation
     *            an Animation object.
     */
    public void run(Animation animation) {
        int millisecondsPerFrame = 1000 / 60;
        long startTime1 = System.currentTimeMillis(); // timing
        while (!animation.shouldStop()) {
            long startTime = System.currentTimeMillis(); // timing
            DrawSurface d = gui.getDrawSurface();

            animation.doOneFrame(d, dt);

            gui.show(d);
            long usedTime = System.currentTimeMillis() - startTime;
            long milliSecondLeftToSleep = millisecondsPerFrame - usedTime;
            if (milliSecondLeftToSleep > 0) {
                this.sleeper.sleepFor(milliSecondLeftToSleep);
            }
        }
    }

    /**
     * @return the GUI object.
     */
    public GUI getGui() {
        return this.gui;
    }
}