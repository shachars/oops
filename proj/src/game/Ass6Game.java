package game;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import animation.AnimationRunner;
import animation.HighScoresAnimation;
import animation.KeyPressStoppableAnimation;
import animation.MenuAnimation;
import animation.Selection;
import animation.Task;
import biuoop.KeyboardSensor;
import levels.LevelSetsReader;

/**
 * class of Ass5Game, contains main method.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Ass6Game {
    /**
     * create list of level according to the args and create a game consists of
     * these levels.
     *
     * @param args
     *            order of levels..
     * @throws Exception
     *             if there is only invalid input.
     */
    public static void main(String[] args) throws Exception {

        final AnimationRunner runner = new AnimationRunner(1.0 / 60);
        final KeyboardSensor keyboard = runner.getGui().getKeyboardSensor();
        final GameFlow game = new GameFlow(runner, keyboard);
        InputStreamReader reader = null;
        if (args.length == 0) {
            InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("level_sets.txt");
            reader = new InputStreamReader(is);
        } else {
            InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(args[0]);
            reader = new InputStreamReader(is);
        }
        Task<Void> quit = new Task<Void>() {

            @Override
            public Void run() {
                System.exit(0);
                return null;
            }
        };
        Task<Void> showHighScores = new Task<Void>() {

            @Override
            public Void run() {
                runner.run(new KeyPressStoppableAnimation(keyboard, KeyboardSensor.SPACE_KEY,
                        new HighScoresAnimation(game.getHst())));
                return null;
            }
        };
        MenuAnimation<Task<Void>> subMenu = new MenuAnimation<Task<Void>>(keyboard, runner);
        LevelSetsReader lsr = new LevelSetsReader();
        List<Selection<Task<Void>>> selections = lsr.fromReader(reader, runner);
        for (Selection<Task<Void>> selection : selections) {
            subMenu.addSelection(selection.getKey(), selection.getMessage(), selection.getReturnVal());
        }
        MenuAnimation<Task<Void>> menu = new MenuAnimation<Task<Void>>(keyboard, runner);
        menu.addSelection("h", "High scores", showHighScores);
        menu.addSubMenu("s", "start a new game", subMenu);
        menu.addSelection("q", "quit the game", quit);

        while (true) {
            runner.run(menu);
            // wait for user selection
            Task<Void> task = menu.getStatus();
            task.run();
            menu.reset();
        }
        ////////
    }
}
