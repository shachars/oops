package game;

/**
 * class of Counter, contains constructor, query and command functions.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Counter {
    // members
    private int counter;

    /**
     * constructor of counter. initialize counter to zero.
     */
    public Counter() {
        this.counter = 0;
    }

    /**
     * add number to current count.
     *
     * @param number
     *            the number to increase
     */
    public void increase(int number) {
        this.counter = this.counter + number;
    }

    /**
     * decrease number from current count.
     *
     * @param number
     *            the number to decrease..
     */
    public void decrease(int number) {
        this.counter = this.counter - number;
    }

    /**
     * @return current count
     */
    public int getValue() {
        return this.counter;
    }

    /**
     * @return if the counter is on zero
     */
    public boolean isZero() {
        return this.counter == 0;
    }

    /**
     * set the value as 0.
     */
    public void makeZero() {
        this.counter = 0;
    }
}