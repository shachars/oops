package game;

import biuoop.DrawSurface;
import sprites.Sprite;

/**
 * class of NameIndicator. contains constructor, query and command functions. in
 * charge of showing the name of the level. implements sprite.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class NameIndicator implements Sprite {
    // members
    private String name;

    /**
     * constructor of NameIndicator.
     *
     * @param name
     *            the name of the level
     */
    public NameIndicator(String name) {
        this.name = name;
    }

    /**
     * draw the name of the level.
     *
     * @param d
     *            a DrawSurface object
     */
    public void drawOn(DrawSurface d) {
        d.drawText(575, 19, "Level Name:" + name, 18);
    }

    /**
     * notify the sprite that time has passed.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void timePassed(double dt) {
        return;
    }

    /**
     * add the indicator to the game.
     *
     * @param g
     *            GameLevel object
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
