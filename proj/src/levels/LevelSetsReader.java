package levels;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

import animation.AnimationRunner;
import animation.Selection;
import animation.Task;
import biuoop.KeyboardSensor;
import game.GameFlow;

/**
 * class of LevelImp, implements LevelInformation, creates LevelInformation
 * object.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class LevelSetsReader {
    // members
    private LineNumberReader lnr;
    private List<Selection<Task<Void>>> selections;

    /**
     * returns list of selections.
     *
     * @param reader
     *            reader of file
     * @param runner
     *            animation runner object
     * @return list of selections
     */
    public List<Selection<Task<Void>>> fromReader(java.io.Reader reader, AnimationRunner runner) {
        final AnimationRunner fRunner = runner;
        String line;
        this.selections = new ArrayList<>();
        this.lnr = new LineNumberReader(reader);
        try {
            while ((line = this.lnr.readLine()) != null) {
                String[] splitted = line.split(":");
                final String filePath = this.lnr.readLine();
                Task<Void> task = new Task<Void>() {

                    @Override
                    public Void run() {
                        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(filePath);
                        InputStreamReader reader = new InputStreamReader(is);
                        LevelSpecificationReader lvlSpecific = new LevelSpecificationReader();
                        List<LevelInformation> readlvlList = lvlSpecific.fromReader(reader);
                        ;
                        KeyboardSensor keyboard = fRunner.getGui().getKeyboardSensor();
                        GameFlow game = new GameFlow(fRunner, keyboard);
                        game.runLevels(readlvlList);
                        return null;
                    }
                };
                this.selections.add(new Selection<Task<Void>>(splitted[0], splitted[1], task));
            }
        } catch (IOException e) {
            System.out.println("problem in LevelSetsReader");
        }
        return this.selections;
    }
}
