package levels;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import animation.ColorsParser;
import biuoop.DrawSurface;
import sprites.Sprite;

/**
 * The Class BackGroundImp.
 */
public class BackGroundImp implements Sprite {

    /** The to create. */
    // members
    private String[] toCreate;

    /**
     * Instantiates a new back ground imp.
     *
     * @param toCreate
     *            the to create
     */
    public BackGroundImp(String toCreate) {
        this.toCreate = toCreate.split("\\(|\\)");
    }

    /**
     * draw the sprite to the screen.
     *
     * @param d
     *            a DrawSurface object
     */
    public void drawOn(DrawSurface d) {
        if (this.toCreate[0].equals("image")) {
            Image img = null;
            try {
                InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(this.toCreate[1]);
                img = ImageIO.read(is);
                d.drawImage(0, 0, img);
            } catch (IOException e) {
                System.out.println("problem with image");
            }
        } else {
            String[] color = this.toCreate[this.toCreate.length - 1].split("\\)");
            d.setColor(ColorsParser.colorFromString(color[0]));
            d.fillRectangle(0, 0, 800, 600);
        }
    }

    /**
     * notify the sprite that time has passed.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void timePassed(double dt) {
        return;
    }
}
