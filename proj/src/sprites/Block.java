package sprites;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import biuoop.DrawSurface;
import blockdefinition.ImageOrColor;
import game.GameLevel;
import geometry.Point;
import geometry.Rectangle;
import listeners.HitListener;
import listeners.HitNotifier;

/**
 * class of block, an implementation of Collidable,Sprite and HitNotifier.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Block implements Collidable, Sprite, HitNotifier {
    // members
    private List<HitListener> hitListeners;
    private Rectangle rect;
    private Map<Integer, ImageOrColor> fillMap;
    private int numberHits;
    private Color stroke;

    /**
     * constructor of block.
     *
     * @param rect
     *            a rectangle.
     * @param fillMap
     *            the colors and images of the block.
     * @param numberHits
     *            the number of hits that the block can take.
     * @param stroke
     *            the the Color of the stroke of the block
     */
    public Block(Rectangle rect, Map<Integer, ImageOrColor> fillMap, int numberHits, Color stroke) {
        // set the members
        this.rect = rect;
        this.fillMap = fillMap;
        this.numberHits = numberHits;
        this.hitListeners = new ArrayList<HitListener>();
        this.stroke = stroke;
    }

    /**
     * @return the Collision Rectangle
     */
    public Rectangle getCollisionRectangle() {
        return this.rect;
    }

    /**
     * calculate the new velocity of the ball after hitting the block.
     *
     * @param hitter
     *            the ball that hit the block
     * @param collisionPoint
     *            the point of the collision.
     * @param currentVelocity
     *            the ball's current velocity.
     * @return the update velocity.
     */
    public Velocity hit(Ball hitter, Point collisionPoint, Velocity currentVelocity) {
        double velDX = currentVelocity.getDX();
        double velDY = currentVelocity.getDY();
        // getting the lineText contains string of the lines that the
        // collision point in them
        String lineText = this.rect.whichLine(collisionPoint);
        // changing the dy or dx according to the lines that the ball hit
        if (lineText.contains("UP")) {
            velDY = -velDY;
        }
        if (lineText.contains("DOWN")) {
            velDY = Math.abs(velDY);
        }
        if (lineText.contains("RIGHT")) {
            velDX = Math.abs(velDX);
        }
        if (lineText.contains("LEFT")) {
            velDX = -velDX;
        }
        // while the numberHits is positive, decreasing the numberHits
        if (this.numberHits > 0) {
            this.numberHits--;
        }
        // notify the sprite that time has passed.
        this.notifyHit(hitter);
        return new Velocity(velDX, velDY);
    }

    /**
     * draw the block.
     *
     * @param d
     *            a DrawSurface object
     */
    public void drawOn(DrawSurface d) {
        // draw the block
        ImageOrColor imgOrclr = this.fillMap.get(this.numberHits);
        if (imgOrclr.isImage()) {
            d.drawImage((int) this.rect.getUpperLeft().getX(), (int) this.rect.getUpperLeft().getY(),
                    imgOrclr.getImage());
        } else {
            d.setColor(imgOrclr.getColor());
            d.fillRectangle((int) this.rect.getUpperLeft().getX(), (int) this.rect.getUpperLeft().getY(),
                    (int) this.rect.getWidth(), (int) this.rect.getHeight());
        }
        if (this.stroke != null) {
            d.setColor(this.stroke);
            d.drawRectangle((int) this.rect.getUpperLeft().getX(), (int) this.rect.getUpperLeft().getY(),
                    (int) this.rect.getWidth(), (int) this.rect.getHeight());
        }
    }

    /**
     * notify the sprite that time has passed.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void timePassed(double dt) {
        return;
    }

    /**
     * add the block to the game.
     *
     * @param g
     *            a Game object.
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
        g.addCollidable(this);
    }

    /**
     * remove the block from the game.
     *
     * @param game
     *            a GameLevel
     */
    public void removeFromGame(GameLevel game) {
        game.removeCollidable(this);
        game.removeSprite(this);
    }

    /**
     * Notify all listeners about hit event.
     *
     * @param hitter
     *            the ball that hit the block.
     */
    private void notifyHit(Ball hitter) {
        // Make a copy of the hitListeners before iterating over them.
        List<HitListener> listeners = new ArrayList<HitListener>(this.hitListeners);
        // Notify all listeners about a hit event:
        for (HitListener hl : listeners) {
            hl.hitEvent(this, hitter);
        }
    }

    /**
     * @return the numberHits of the block
     */
    public int getHitPoints() {
        return this.numberHits;
    }

    /**
     * add Listener to the hitListeners list.
     *
     * @param hl
     *            HitListener
     */
    public void addHitListener(HitListener hl) {
        this.hitListeners.add(hl);
    }

    /**
     * remove Listener from the hitListeners list.
     *
     * @param hl
     *            HitListener
     */
    public void removeHitListener(HitListener hl) {
        this.hitListeners.remove(hl);
    }
}