package animation;

import score.ScoreInfo;
import score.HighScoresTable;

import java.awt.Color;
import java.util.Iterator;

import biuoop.DrawSurface;

/**
 * class HighScoresAnimation. implements Animation. draw one frame of the High
 * Scores Table.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class HighScoresAnimation implements Animation {
    private boolean stop;
    private HighScoresTable scores;

    /**
     * constructor of HighScoresAnimation. get HighScoresTable and endKey.
     *
     * @param scores
     *            a HighScoresTable object
     */
    public HighScoresAnimation(HighScoresTable scores) {
        this.scores = scores;
        this.stop = false;
    }

    /**
     * show one frame of the high scores.
     *
     * @param d
     *            a DrawSurface object
     * @param dt
     *            seconds passed since last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        d.setColor(Color.BLUE);
        d.drawText(300, 50, "High Scores", 50);
        d.setColor(Color.RED);
        int index = 1;
        for (Iterator<ScoreInfo> iterator = scores.getHighScores().iterator(); iterator.hasNext();) {
            ScoreInfo score = (ScoreInfo) iterator.next();
            d.drawText(200, 90 + index * 50, index + ". " + score.getName(), 40);
            d.drawText(500, 90 + index * 50, Integer.toString(score.getScore()), 40);
            index++;
        }
        d.drawText(150, 550, "prese space to return to the menu", 28);
    }

    /**
     * @return running situation
     */
    public boolean shouldStop() {
        return this.stop;
    }
}
