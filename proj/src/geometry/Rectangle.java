package geometry;
import java.util.ArrayList;

/**
 * class of Rectangle. has constructor and get functions. can calculate the
 * intersection points with a line and which line the intersection point is in.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Rectangle {
    // members
    private Point upperLeft;
    private double width;
    private double height;
    // array of the rectangle's edge
    private Line[] lineArray;

    /**
     * constructor of Rectangle.
     *
     * @param upperLeft
     *            the upperLeft point.
     * @param width
     *            the width of the rectangle.
     * @param height
     *            the height of the rectangle.
     *
     */
    public Rectangle(Point upperLeft, double width, double height) {
        // set members
        this.height = height;
        this.width = width;
        this.upperLeft = upperLeft;
        // create the edges of the rectangle
        Point upperRight = new Point(this.upperLeft.getX() + this.width, this.upperLeft.getY());
        Point lowerLeft = new Point(this.upperLeft.getX(), this.upperLeft.getY() + this.height);
        Point lowerRight = new Point(this.upperLeft.getX() + this.width, this.upperLeft.getY() + this.height);
        Line upLine = new Line(this.upperLeft, upperRight);
        Line lowLine = new Line(lowerLeft, lowerRight);
        Line rightLine = new Line(upperRight, lowerRight);
        Line leftLine = new Line(this.upperLeft, lowerLeft);
        // create an array of the edges
        this.lineArray = new Line[] {upLine, lowLine, rightLine, leftLine};
    }

    /**
     * create a list of all the intersection Points of the rectangle with a
     * line.
     *
     * @param line
     *            a line.
     * @return list of intersection Points
     */
    public java.util.List intersectionPoints(Line line) {
        ArrayList listOfPoints = new ArrayList();
        // create a (possibly empty) List of intersection points
        // with the specified line.
        for (int i = 0; i < lineArray.length; i++) {
            if (line.isIntersecting(lineArray[i])) {
                listOfPoints.add(line.intersectionWith(lineArray[i]));
            }
        }
        return listOfPoints;
    }

    /**
     * create a string contains the lines of the collision point.
     *
     * @param collisionPoint
     *            a point of collision between the rectangle and a ball.
     * @return a string contains the line(s) that point is in them.
     */
    public String whichLine(Point collisionPoint) {
        // create empty string
        String lineText = "";
        // check which line the collision point in
        for (int i = 0; i < lineArray.length; i++) {
            Equation equaition = new Equation(lineArray[i]);
            // check whether the point is in the specific line
            // if the point is in the specific line, add the line to the string
            if (equaition.isOnEquation(collisionPoint)) {
                switch (i) {
                case 0:
                    lineText = lineText + "UP ";
                    break;
                case 1:
                    lineText = lineText + "DOWN ";
                    break;
                case 2:
                    lineText = lineText + "RIGHT ";
                    break;
                case 3:
                    lineText = lineText + "LEFT ";
                    break;
                default:
                    break;
                }
            }
        }
        return lineText;
    }

    /**
     * @return the width of the rectangle.
     */
    // Return the width and height of the rectangle
    public double getWidth() {
        return this.width;
    }

    /**
     * @return the height of the rectangle.
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * @return the upperLeft point of the rectangle.
     */
    // Returns the upper-left point of the rectangle.
    public Point getUpperLeft() {
        return this.upperLeft;
    }
}