package levels;

import java.util.List;

import sprites.Block;
import sprites.Sprite;
import sprites.Velocity;

/**
 * class of LevelImp, implements LevelInformation, creates LevelInformation
 * object.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class LevelImp implements LevelInformation {
    // members
    private int numberOfBalls;
    private List<Velocity> ballVelocities;
    private int paddleSpeed;
    private int paddleWidth;
    private String levelName;
    private Sprite background;
    private List<Block> blocks;
    private int numberOfBlocksToRemove;

    /**
     * constructor of LevelImp.
     *
     * @param numberOfBalls
     *            number of balls in the level
     * @param ballVelocities
     *            velocities of the balls
     * @param paddleValues
     *            values of the paddle
     * @param levelName
     *            name of level
     * @param background
     *            background of level
     * @param blocks
     *            list of blocks to create
     * @param numberOfBlocksToRemove
     *            number Of Blocks To Remove
     */
    public LevelImp(int numberOfBalls, List<Velocity> ballVelocities,
            int[] paddleValues, String levelName, Sprite background,
            List<Block> blocks, int numberOfBlocksToRemove) {
        this.numberOfBalls = numberOfBalls;
        this.ballVelocities = ballVelocities;
        this.paddleSpeed = paddleValues[0];
        this.paddleWidth = paddleValues[1];
        this.levelName = levelName;
        this.background = background;
        this.blocks = blocks;
        this.numberOfBlocksToRemove = numberOfBlocksToRemove;
    }

    /**
     * @return the number of balls in the game.
     */
    public int numberOfBalls() {
        return this.numberOfBalls;
    }

    /**
     * insert the velocities of the balls to a list and return it.
     *
     * @return list of velocities.
     */
    public List<Velocity> initialBallVelocities() {
        return this.ballVelocities;
    }

    /**
     * @return the speed of the paddle.
     */
    public int paddleSpeed() {
        return this.paddleSpeed;
    }

    /**
     * @return the width of the paddle.
     */
    public int paddleWidth() {
        return this.paddleWidth;
    }

    /**
     * @return the name of the level.
     */
    public String levelName() {
        return this.levelName;
    }

    /**
     * @return the background of the level.
     */
    public Sprite getBackground() {
        return this.background;
    }

    /**
     * create a list, contains the blocks of this level and return it.
     *
     * @return list of the blocks.
     */
    public List<Block> blocks() {
        return this.blocks;
    }

    /**
     * @return number of the blocks needed to be remove for ending the game.
     */
    public int numberOfBlocksToRemove() {
        return this.numberOfBlocksToRemove;
    }

}
