/**
* @author Shachar sirotkin.
* version 1.7
* @since 03-03-2016
*/
public class Factorial {
    /**
    * print the factorial by using recursive and iterative functions.
    * @param args Array contains one number.
    */
    public static void main(String[] args) {
        //right input is only one number
        if (args.length != 1) {
            System.out.println("sorry,Wrong input");
            return;
        }
        int number = Integer.parseInt(args[0]);
        System.out.println("recursive: " + factorialRecursive(number));
        System.out.println("iterative: " + factorialIter(number));
    }
    /**
    * calculate the factorial in iterative way.
    * @param n number.
    * @return the factorial of n.
    */
    public static long factorialIter(long n) {
        if (n == 0) {
            return 1;
        }
        long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
    /**
    * calculate the factorial in recursive way.
    * @param n number.
    * @return the factorial of n.
    */
    public static long factorialRecursive(long n) {
        if (n == 0) {
            return 1;
        }
        return n * factorialRecursive(n - 1);
    }
}