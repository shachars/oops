package animation;

import biuoop.DrawSurface;

/**
 * class of EndScreen. shows the pause screen. implements Animation.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class PauseScreen implements Animation {
    private boolean stop;

    /**
     * constructor of PauseScreen.
     */
    public PauseScreen() {
        this.stop = false;
    }

    /**
     * show one frame of the end screen.
     *
     * @param d
     *            a DrawSurface object
     * @param dt
     *            seconds passed since last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        d.drawText(10, d.getHeight() / 2, "paused -- press space to continue", 32);
    }

    /**
     * @return running situation.
     */
    public boolean shouldStop() {
        return this.stop;
    }
}
