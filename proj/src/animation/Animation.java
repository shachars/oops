package animation;

import biuoop.DrawSurface;

/**
 * interface of Animation.
 *
 * @author Shachar & Yehoshua.
 *
 */
public interface Animation {
    /**
     * show one frame of the specific animation.
     *
     * @param d
     *            a DrawSurface object
     * @param dt
     *            seconds passed since last call.
     */
    void doOneFrame(DrawSurface d, double dt);

    /**
     * @return running situation.
     */
    boolean shouldStop();
}