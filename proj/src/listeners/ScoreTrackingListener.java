package listeners;

import game.Counter;
import sprites.Ball;
import sprites.Block;

/**
 * Class of ScoreTrackingListener. implements HitListener. count the score that
 * the player earn in the game.
 *
 * @author Shachar & Yehoshua
 *
 */
public class ScoreTrackingListener implements HitListener {
    private Counter currentScore;

    /**
     * constructor of ScoreTrackingListener.
     *
     * @param scoreCounter
     *            a Counter of the score the player earn in the game.
     */
    public ScoreTrackingListener(Counter scoreCounter) {
        this.currentScore = scoreCounter;
    }

    /**
     * count the score that the player earn in the game. any hit is 5 points and
     * destroying block is 10 points.
     *
     * @param beingHit
     *            the notifier.
     * @param hitter
     *            the ball that hit the notifier.
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        this.currentScore.increase(5);
        if (beingHit.getHitPoints() == 0) {
            this.currentScore.increase(10);
        }
    }
}
