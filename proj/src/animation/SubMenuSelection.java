package animation;

/**
 * class of SubMenuSelection, defines sub menu Selection.
 *
 * @author Shachar & Yehoshua.
 *
 * @param <T>
 *            generic value for sub menu Selection
 */
public class SubMenuSelection<T> {
    // members
    private Menu<T> subMenu;
    private String key;
    private String message;

    /**
     * constructor of Selection.
     *
     * @param key
     *            key to press
     * @param message
     *            message to show
     * @param subMenu
     *            a sub menu
     */
    public SubMenuSelection(String key, String message, Menu<T> subMenu) {
        // members.
        this.key = key;
        this.message = message;
        this.subMenu = subMenu;
    }

    /**
     * @return key of sub menu Selection
     */
    public String getKey() {
        return this.key;
    }

    /**
     * @return message of sub menu Selection
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @return return value of sub menu Selection
     */
    public Menu<T> getSubMenu() {
        return this.subMenu;
    }
}
