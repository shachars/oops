package animation;

/**
 * interface of MMenu<T>, extends Animation.
 *
 * @param <T>
 *            generic type
 * @author Shachar & Yehoshua.
 *
 */
public interface Menu<T> extends Animation {
    /**
     * add selection to selection list.
     *
     * @param key
     *            key to press
     * @param message
     *            message to show
     * @param returnVal
     *            value to return
     */
    void addSelection(String key, String message, T returnVal);

    /**
     * @return the status according to the key pressed
     */
    T getStatus();

    /**
     * add sub menu.
     *
     * @param key
     *            key to press
     * @param message
     *            message to show
     * @param subMenu
     *            a sub menu
     */
    void addSubMenu(String key, String message, Menu<T> subMenu);

    /**
     * reset the stopper.
     */
    void reset();
}
