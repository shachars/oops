package score;

import java.io.Serializable;

/**
 * Class of ScoreInfo.Serializable class. contains the details of one score :
 * the name and the score.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class ScoreInfo implements Serializable {
    // members
    private static final long serialVersionUID = -2796283078367421260L;
    private String name;
    private int score;

    /**
     * constructor of ScoreInfo.
     *
     * @param name
     *            the name of the player.
     * @param score
     *            the final score.
     */
    public ScoreInfo(String name, int score) {
        this.name = name;
        this.score = score;
    }

    /**
     * @return the name of the player.
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return the score of the player.
     */
    public int getScore() {
        return this.score;
    }
}