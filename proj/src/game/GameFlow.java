package game;

import java.io.File;
import java.util.List;

import animation.AnimationRunner;
import animation.EndScreen;
import animation.HighScoresAnimation;
import animation.KeyPressStoppableAnimation;
import biuoop.DialogManager;
import biuoop.KeyboardSensor;
import levels.LevelInformation;
import score.HighScoresTable;
import score.ScoreInfo;

/**
 * class of GameFlow. in charge of running the levels.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class GameFlow {
    // members
    private AnimationRunner runner;
    private KeyboardSensor keySensor;
    private Counter scoreCounter;
    private Counter livesCounter;
    private File filename;
    private HighScoresTable hst;

    /**
     * constructor of GameFlow.
     *
     * @param ar
     *            object of AnimationRunner
     * @param ks
     *            object of KeyboardSensor
     */
    public GameFlow(AnimationRunner ar, KeyboardSensor ks) {
        this.keySensor = ks;
        this.runner = ar;
        this.livesCounter = new Counter();
        this.scoreCounter = new Counter();
        this.filename = new File("highscores.txt");
        this.hst = HighScoresTable.loadFromFile(filename);
        // this.hst = new HighScoresTable(8);
    }

    /**
     * run the levels in levels list..
     *
     * @param levels
     *            list of levels to run..
     */
    public void runLevels(List<LevelInformation> levels) {
        // File filename = new File("highscores.txt");
        // this.hst = HighScoresTable.loadFromFile(filename);
        this.scoreCounter.makeZero();
        this.livesCounter.increase(7);
        for (LevelInformation levelInfo : levels) {
            // run each level
            GameLevel level = new GameLevel(levelInfo, this.scoreCounter, this.livesCounter, this.runner,
                    this.keySensor);
            level.initialize();
            // loop of one turn of the game until the player lose or there are
            // no blocks
            while (!this.livesCounter.isZero() && !level.getBlocksCounter().isZero()) {
                level.playOneTurn();
            }

            if (this.livesCounter.isZero()) {
                break;
            }
        }
        // ask the player for his name
        if (hst.getRank(scoreCounter.getValue()) < hst.size()) {
            DialogManager dialog = this.runner.getGui().getDialogManager();
            String name = dialog.showQuestionDialog("Name", "What is your name?", "");
            ScoreInfo si = new ScoreInfo(name, this.scoreCounter.getValue());
            this.hst.add(si);
        }
        this.hst.save(this.filename);
        // show the endscreen
        this.runner.run(new KeyPressStoppableAnimation(this.keySensor, KeyboardSensor.SPACE_KEY,
                new EndScreen(this.livesCounter, this.scoreCounter)));
        // show the high scores table and close the gui.
        this.runner.run(new KeyPressStoppableAnimation(this.keySensor, KeyboardSensor.SPACE_KEY,
                new HighScoresAnimation(this.hst)));
        // this.runner.getGui().close();
    }

    /**
     * @return the High Scores Table.
     */
    public HighScoresTable getHst() {
        return this.hst;
    }
}
