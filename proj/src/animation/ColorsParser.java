package animation;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
/**
 * class of ColorsParser. parse a Color from a string.
 * @author Shachar & Yehoshua.
 *
 */
public class ColorsParser {
    /**
     * return the color that represented by a String.
     *
     * @param s
     *            a String that represents a Color.
     * @return the color that represented by a String.
     */
    public static java.awt.Color colorFromString(String s) {
        if (s.contains(",")) {
            String[] split = s.split(",");
            return new Color(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
        }
        Map<String, Color> colorMap = new HashMap<String, Color>();
        colorMap.put("black", Color.BLACK);
        colorMap.put("blue", Color.BLUE);
        colorMap.put("cyan", Color.CYAN);
        colorMap.put("gray", Color.GRAY);
        colorMap.put("lightGray", Color.LIGHT_GRAY);
        colorMap.put("green", Color.GREEN);
        colorMap.put("orange", Color.ORANGE);
        colorMap.put("pink", Color.PINK);
        colorMap.put("red", Color.RED);
        colorMap.put("white", Color.WHITE);
        colorMap.put("yellow", Color.YELLOW);
        return colorMap.get(s);
    }
}
