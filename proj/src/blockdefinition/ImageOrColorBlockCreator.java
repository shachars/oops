package blockdefinition;

import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import animation.ColorsParser;
import geometry.Point;
import geometry.Rectangle;
import sprites.Block;

/**
 * class of ImageOrColorBlockCreator. implements BlockCreator. can create a
 * block according to a block line.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class ImageOrColorBlockCreator implements BlockCreator {
    // members.
    private Map<String, String> blockCharacts;
    private int width;
    private int height;
    private int numberHits;
    private Map<Integer, ImageOrColor> fillMap;
    private Color stroke;

    /**
     * constructor of ImageOrColorBlockCreator. set the numberHits and create
     * the fillMap of the block.
     *
     * @param blockCharacts
     *            a Map of all the characters and values of the block.
     */
    public ImageOrColorBlockCreator(Map<String, String> blockCharacts) {
        this.blockCharacts = blockCharacts;
        this.numberHits = Integer.parseInt(this.blockCharacts.get("hit_points"));
        this.fillMap = getFillMap();
        getStroke();
    }

    /**
     * set the stroke of the block.
     */
    private void getStroke() {
        String strokeString = null;
        if (this.blockCharacts.containsKey("stroke")) {
            strokeString = this.blockCharacts.get("stroke");
            this.stroke = getColor(strokeString);
        }
    }

    /**
     * return the image that the string represents.
     *
     * @param fillString
     *            a string contains a fill that represents an image.
     * @return the image that the string represents.
     */
    private Image getimage(String fillString) {
        Image img = null;
        fillString = fillString.split("\\(")[1];
        String imageStr = fillString.substring(0, fillString.length() - 1);
        try {
            InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(imageStr);
            img = ImageIO.read(is);
        } catch (IOException e) {
            System.out.println("problem with block image");
        }
        return img;
    }

    /**
     * Create a block at the specified location.
     *
     * @param xpos
     *            the x coordinate of the rectangle
     * @param ypos
     *            the y coordinate of the rectangle
     * @return a block in these coordinates
     */
    public Block create(int xpos, int ypos) {
        Rectangle rect = setRect(xpos, ypos);
        return new Block(rect, this.fillMap, this.numberHits, this.stroke);
    }

    /**
     * return the color that the string represents.
     *
     * @param fillString
     *            a string contains a fill that represents a color.
     * @return the color that the string represents.
     */
    private Color getColor(String fillString) {
        String colorStr;
        Color cl;
        if (fillString.contains("RGB")) {
            fillString = fillString.split("\\(")[2];
            colorStr = fillString.substring(0, fillString.length() - 3);
            cl = ColorsParser.colorFromString(colorStr);
        } else {
            fillString = fillString.split("\\(")[1];
            colorStr = fillString.substring(0, fillString.length() - 1);
            cl = ColorsParser.colorFromString(colorStr);
        }
        return cl;
    }

    /**
     * @return a map that maps the number of hits left to the "fill"s of the
     *         block
     */
    private Map<Integer, ImageOrColor> getFillMap() {
        Map<Integer, ImageOrColor> tempfillMap = new HashMap<Integer, ImageOrColor>();
        String fillString;
        int index = this.numberHits;
        for (int i = this.numberHits; i > 0; i--) {
            String fillKey = "fill-" + Integer.toString(i);
            if (this.blockCharacts.containsKey(fillKey)) {
                fillString = this.blockCharacts.get(fillKey);
                addtoFillMap(i, fillString, tempfillMap);
                index--;
            } else {
                break;
            }
        }
        while (index > 0) {
            if (this.blockCharacts.containsKey("fill")) {
                fillString = this.blockCharacts.get("fill");
                addtoFillMap(index, fillString, tempfillMap);
            }
            index--;
        }
        return tempfillMap;
    }

    /**
     * add one fill to the fillMap.
     *
     * @param fillKey
     *            the number that is the key to the current "fill"
     * @param fillString
     *            the string that represents the "fill"
     * @param tempFillMap
     *            the map of the "fill"s
     */
    private void addtoFillMap(int fillKey, String fillString, Map<Integer, ImageOrColor> tempFillMap) {
        if (fillString.contains("color")) {
            ImageOrColor color = new ImageOrColor(getColor(fillString));
            tempFillMap.put(fillKey, color);
        } else {
            ImageOrColor image = new ImageOrColor(getimage(fillString));
            tempFillMap.put(fillKey, image);
        }
    }

    /**
     * create the block's rectangle.
     *
     * @param xpos
     *            the x coordinate of the rectangle
     * @param ypos
     *            the y coordinate of the rectangle
     * @return the block's rectangle.
     */
    private Rectangle setRect(int xpos, int ypos) {
        Point upperLeft = new Point(xpos, ypos);
        this.width = Integer.parseInt(this.blockCharacts.get("width"));
        this.height = Integer.parseInt(this.blockCharacts.get("height"));
        return new Rectangle(upperLeft, this.width, this.height);
    }

}
