package game;

import java.awt.Color;

import biuoop.DrawSurface;
import sprites.Sprite;

/**
 * class of ScoreIndicator. contains constructor, query and command functions.
 * in charge of showing the score. implements sprite.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class ScoreIndicator implements Sprite {
    // members
    private Counter scoreCounter;

    /**
     * constructor of ScoreIndicator.
     *
     * @param scoreCounter
     *            Counter object that counts the lives.
     */
    public ScoreIndicator(Counter scoreCounter) {
        this.scoreCounter = scoreCounter;
    }

    /**
     * draw the lives that left.
     *
     * @param d
     *            a DrawSurface object
     */
    public void drawOn(DrawSurface d) {
        d.setColor(Color.WHITE);
        d.fillRectangle(0, 0, 800, 20);
        d.setColor(Color.BLACK);
        d.drawText(375, 19, "score: " + scoreCounter.getValue(), 18);
    }

    /**
     * notify the sprite that time has passed.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void timePassed(double dt) {
        return;
    }

    /**
     * add the indicator to the game.
     *
     * @param g
     *            GameLevel object
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }
}
