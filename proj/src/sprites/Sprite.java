package sprites;

import biuoop.DrawSurface;

/**
 * Interface of Sprite. contains drawOn and timePassed functions.
 *
 * @author Shachar & Yehoshua.
 *
 */
public interface Sprite {
    /**
     * draw the sprite to the screen.
     *
     * @param d
     *            a DrawSurface object
     */
    void drawOn(DrawSurface d);

    /**
     * notify the sprite that time has passed.
     *
     * @param dt
     *            seconds passed since last call.
     */
    void timePassed(double dt);
}
