package listeners;

/**
 * interface of HitNotifier. has add and remove function.
 *
 * @author Shachar & Yehoshua
 *
 */
public interface HitNotifier {
    /**
     * Add hl as a listener to hit events.
     *
     * @param hl
     *            a HitListener
     */
    void addHitListener(HitListener hl);

    /**
     * Remove hl from the list of listeners to hit events.
     *
     * @param hl
     *            a HitListener
     */
    void removeHitListener(HitListener hl);
}