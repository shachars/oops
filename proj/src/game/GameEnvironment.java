package game;
import java.util.ArrayList;
import java.util.List;

import geometry.Line;
import geometry.Point;
import sprites.Collidable;
import sprites.CollisionInfo;

/**
 * class of gameEnviroment. contains a list of all the collidables in the
 * surface. can find the closest collision.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class GameEnvironment {
    // members
    private List collidablesList;

    /**
     * auto constructor.
     */
    public GameEnvironment() {
        // create empty list
        this.collidablesList = new ArrayList();
    }

    /**
     * adding Collidable object to the list.
     *
     * @param c
     *            a Collidable object.
     */
    public void addCollidable(Collidable c) {
        this.collidablesList.add(c);
    }
    /**
     * remove collidable from game.
     *
     * @param c
     *            the collidable to remove
     */
    public void removeCollidable(Collidable c) {
        this.collidablesList.remove(c);
    }

    /***
     * find the Closest Collision and create matching CollisionInfo.
     *
     * @param trajectory
     *            the line of the trajectory of the ball without obstacles.
     * @return CollisionInfo of the Closest Collision.
     */
    public CollisionInfo getClosestCollision(Line trajectory) {
        Collidable c;
        Collidable closestCollide = null;
        int i;
        Point closestCollisionPoint = null;
        // find the closest collision.
        for (i = 0; i < this.collidablesList.size(); i++) {
            closestCollide = (Collidable) this.collidablesList.get(i);
            closestCollisionPoint = trajectory.closestIntersectionToStartOfLine(
                    closestCollide.getCollisionRectangle());
            if (closestCollisionPoint != null) {
                break;
            }
        }
        // no collision found
        if (closestCollisionPoint == null) {
            return null;
        }
        // search for the closest collision by distance
        for (int j = i; j < this.collidablesList.size(); j++) {
            c = (Collidable) this.collidablesList.get(j);
            Point colllisionPoint = trajectory.closestIntersectionToStartOfLine(
                    c.getCollisionRectangle());
            // null point has no distance
            if (colllisionPoint == null) {
                continue;
            }
            if (trajectory.start().distance(colllisionPoint) < trajectory
                    .start().distance(closestCollisionPoint)) {
                closestCollisionPoint = colllisionPoint;
                closestCollide = c;
            }
        }
        return new CollisionInfo(closestCollisionPoint, closestCollide);
    }

    /**
     * @return the list of the collidables.
     */
    public java.util.List getCollidablesList() {
        return this.collidablesList;
    }
}