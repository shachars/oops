package listeners;

import sprites.Ball;
import sprites.Block;

/**
 * interface of HitListener. create implementation of the Listeners Pattern.
 *
 * @author Shachar & Yehoshua.
 *
 */
public interface HitListener {
    /**
     * This method is called whenever the beingHit object is hit. The hitter
     * parameter is the Ball that's doing the hitting.
     *
     * @param beingHit
     *            the notifier.
     * @param hitter
     *            the ball that hit the notifier.
     */
    void hitEvent(Block beingHit, Ball hitter);
}