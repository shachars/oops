package levels;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import blockdefinition.BlocksDefinitionReader;
import blockdefinition.BlocksFromSymbolsFactory;
import sprites.Block;
import sprites.Sprite;
import sprites.Velocity;

/**
 * The Class LevelSpecificationReader. read from level set file.
 */
public class LevelSpecificationReader {
    /** The br. */
    // members.
    private BufferedReader br;

    /**
     * From reader.
     *
     * @param reader
     *            the reader object of level sets
     * @return the list of level information
     */
    public List<LevelInformation> fromReader(java.io.Reader reader) {
        List<LevelInformation> levelInformationList = new ArrayList<>();
        this.br = new BufferedReader(reader);
        try {
            String line = br.readLine();
            while (line != null) {
                List<String> oneLevelList = createListOfLines(reader);
                levelInformationList.add(createLevelInformation(oneLevelList));
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return levelInformationList;
    }

    /**
     * Creates the list of lines.
     *
     * @param reader
     *            the reader object of level sets
     * @return the list of one level strings
     */
    public List<String> createListOfLines(java.io.Reader reader) {
        List<String> oneLevelList = new ArrayList<>();
        String line;
        try {
            line = this.br.readLine();
            while (!line.equals("END_LEVEL")) {
                oneLevelList.add(line);
                line = this.br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return oneLevelList;
    }

    /**
     * Creates the level information.
     *
     * @param oneLevelList
     *            the one level list of strings
     * @return the level information object
     */
    public LevelInformation createLevelInformation(List<String> oneLevelList) {
        int[] paddleValues = new int[2];
        int fromIndex = oneLevelList.indexOf("START_BLOCKS");
        int toIndex = oneLevelList.indexOf("END_BLOCKS");
        List<String> blockStringList = new ArrayList<>();
        blockStringList = oneLevelList.subList(fromIndex + 1, toIndex);
        String part1 = "", part2 = "", levelName = "";
        int numberOfBalls = 0, numberOfBlocksToRemove = 0;
        Sprite background = null;
        List<Velocity> ballVelocities = null;
        int xpos = 0, ypos = 0, rowHeight = 0;
        String filePath = "";
        for (String string : oneLevelList) {
            if (string.contains(":")) {
                String[] parts = string.split(":");
                part1 = parts[0];
                part2 = parts[1];
            }
            switch (part1) {
            case "level_name":
                levelName = part2;
                break;
            case "ball_velocities"://///
                ballVelocities = createBallVelocities(part2);
                break;
            case "background"://///
                background = createBackground(part2);
                break;
            case "paddle_speed":
                paddleValues[0] = createPaddleSpeed(part2);
                break;
            case "paddle_width":
                paddleValues[1] = createPaddleWidth(part2);
                break;
            case "num_blocks":
                numberOfBlocksToRemove = createNumberOfBlocksToRemove(part2);
                break;
            case "blocks_start_x":
                xpos = Integer.parseInt(part2);
                break;
            case "blocks_start_y":
                ypos = Integer.parseInt(part2);
                break;
            case "row_height":
                rowHeight = Integer.parseInt(part2);
                break;
            case "block_definitions":
                filePath = part2;
                break;
            default:
                break;
            }
        }
        numberOfBalls = ballVelocities.size();
        List<Block> blocks = createBlocks(filePath, blockStringList, xpos, ypos, rowHeight);
        return new LevelImp(numberOfBalls, ballVelocities, paddleValues, levelName, background, blocks,
                numberOfBlocksToRemove);
    }

    /**
     * Creates the number of blocks to remove.
     *
     * @param part2
     *            the part2 of the string
     * @return the int value of part2 of the string
     */
    private int createNumberOfBlocksToRemove(String part2) {
        return Integer.parseInt(part2);
    }

    /**
     * Creates the blocks.
     *
     * @param part2
     *            the part2 of the string
     * @param blockStringList
     *            the block string list
     * @param xpos
     *            the x position
     * @param ypos
     *            the y position.
     * @param rowHeight
     *            the row height
     * @return the list of blocks to create.
     */
    private List<Block> createBlocks(String part2, List<String> blockStringList, int xpos, int ypos, int rowHeight) {
        List<Block> listOfBlocks = new ArrayList<>();
        int initialXPos = xpos;
        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(part2);
        InputStreamReader blockReader = new InputStreamReader(is);
        BlocksFromSymbolsFactory bfsf = BlocksDefinitionReader.fromReader(blockReader);
        for (String string : blockStringList) {
            int index = 0;
            char[] fromString = string.toCharArray();
            xpos = initialXPos;
            for (int i = index; i < fromString.length; i++) {
                if (bfsf.isBlockSymbol(Character.toString(fromString[i]))) {
                    Block b = bfsf.getBlock(Character.toString(fromString[i]), xpos, ypos);
                    listOfBlocks.add(b);
                    xpos += b.getCollisionRectangle().getWidth();
                }
                if (bfsf.isSpaceSymbol(Character.toString(fromString[i]))) {
                    xpos += bfsf.getSpaceWidth(Character.toString(fromString[i]));
                }
            }
            ypos += rowHeight;
        }
        return listOfBlocks;
    }

    /**
     * Creates the background.
     *
     * @param part2
     *            the part2 of the string
     * @return the implementation of background
     */
    private Sprite createBackground(String part2) {
        return new BackGroundImp(part2);
    }

    /**
     * Creates the paddle width.
     *
     * @param part2
     *            the part2 of the string
     * @return the int value of part2 of the string
     */
    private int createPaddleWidth(String part2) {
        return Integer.parseInt(part2);
    }

    /**
     * Creates the paddle speed.
     *
     * @param part2
     *            the part2 of the string
     * @return the int value of part2 of the string
     */
    private int createPaddleSpeed(String part2) {
        return Integer.parseInt(part2);
    }

    /**
     * Creates the ball velocities.
     *
     * @param velocities
     *            the velocities of the balls in string
     * @return the list of the velocities of the balls
     */
    private List<Velocity> createBallVelocities(String velocities) {
        String[] split = velocities.split(" ");
        List<Velocity> velList = new ArrayList<>();
        for (int i = 0; i < split.length; i++) {
            String[] split2 = split[i].split(",");
            int angle = Integer.parseInt(split2[0]);
            int speed = Integer.parseInt(split2[1]);
            velList.add(Velocity.fromAngleAndSpeed(angle, speed));
        }
        return velList;
    }

}