package blockdefinition;

import sprites.Block;

/**
 * Interface of BlockCreator. has function that create a block.
 *
 * @author Shachar & Yehoshua.
 *
 */
public interface BlockCreator {
    /**
     * Create a block at the specified location.
     *
     * @param xpos
     *            the x coordinate of the rectangle
     * @param ypos
     *            the y coordinate of the rectangle
     * @return a block in these coordinates
     */
    Block create(int xpos, int ypos);
}
