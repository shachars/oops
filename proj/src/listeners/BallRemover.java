package listeners;

import game.Counter;
import game.GameLevel;
import sprites.Ball;
import sprites.Block;

/**
 *
 * class of BallRemover. implements HitListener. remove a ball when it hits a
 * block that notify this Listener.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class BallRemover implements HitListener {
    private GameLevel game;
    private Counter remainingBall;

    /**
     * Constructor of BallRemover.
     *
     * @param game
     *            a GameLevel.
     * @param remainBall
     *            the Counter of the balls remain in the game.
     */
    public BallRemover(GameLevel game, Counter remainBall) {
        this.game = game;
        this.remainingBall = remainBall;
    }

    /**
     * remove the hitter ball from the game and decreasing the counter by 1.
     *
     * @param beingHit
     *            the notifier.
     * @param hitter
     *            the ball that hit the notifier.
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        hitter.removeFromGame(this.game);
        this.remainingBall.decrease(1);
    }

}
