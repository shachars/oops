/**
* @author Shachar sirotkin.
* version 1.7
* @since 03-03-2016
*/
public class Sort {
    /**
    * sort and print the numbers from args
    * in ascending or descending order.
    * @param args Array contains order type and numbers.
    */
    public static void main(String[] args) {
        //no args is wrong input
        if (args.length == 0) {
            System.out.println("sorry,Wrong input");
            return;
        }
        int[] numbers = stringsToInts(args);
        if (args[0].equals("asc")) {
            ascendOrder(numbers);
            printArray(numbers);
            return;
        }
        if (args[0].equals("desc")) {
            descendOrder(numbers);
            printArray(numbers);
        } else {
                System.out.println("sorry,Wrong input");
        }
    }
    /**
    * sort the array in ascending order.
    * @param numbers Array of numbers.
    */
    public static void ascendOrder(int[] numbers) {
        //sorting the array using bubble sort
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - 1 - i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }
    }
    /**
    * sort the array in descending order.
    * @param numbers Array of numbers.
    */
    public static void descendOrder(int[] numbers) {
        //sorting the array using bubble sort
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - 1 - i; j++) {
                if (numbers[j] < numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }
    }
    /**
    * convert the array of strings to array of integers.
    * @param numbers Array of strings of numbers.
    * @return convertedArray array of integers.
    */
    public static int[] stringsToInts(String[] numbers) {
        int [] convertedArray = new int[numbers.length - 1];
        for (int i = 1; i < numbers.length; i++) {
            convertedArray[i - 1] = Integer.parseInt(numbers[i]);
        }
        return convertedArray;
    }
    /**
    * print the array.
    * @param numbers Array of numbers.
    */
    public static void printArray(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.print("\n");
    }
}