package listeners;

import game.Counter;
import game.GameLevel;
import sprites.Ball;
import sprites.Block;

/**
 * Class of BlockRemover.implements HitListener.remove blocks from the game, and
 * count the number of blocks that remain.
 */
public class BlockRemover implements HitListener {
    private GameLevel game;
    private Counter remainingBlocks;

    /**
     * constructor of BlockRemover.
     *
     * @param game
     *            a GameLevel.
     * @param remainBlocks
     *            a Counter of the blocks remain in the game.
     */
    public BlockRemover(GameLevel game, Counter remainBlocks) {
        this.game = game;
        this.remainingBlocks = remainBlocks;
    }

    /**
     * remove the beingHit block from the game when its HitPoints become 0 and
     * decreasing the counter by 1.
     *
     * @param beingHit
     *            the notifier.
     * @param hitter
     *            the ball that hit the notifier.
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        if (beingHit.getHitPoints() == 0) {
            beingHit.removeFromGame(this.game);
            beingHit.removeHitListener(this);
            this.remainingBlocks.decrease(1);
        }
    }
}