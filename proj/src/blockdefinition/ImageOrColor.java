package blockdefinition;

import java.awt.Color;
import java.awt.Image;

/**
 * class of ImageOrColor. can contain a value of Color or Image but not the both
 * of them. has a functions that can tell which type of value is stored in the
 * variable.
 *
 * @author @author Shachar & Yehoshua.
 *
 */
public class ImageOrColor {
    private Image img;
    private Color clr;
    private boolean isImage;
    private boolean isColor;

    /**
     * constructor that get an Image. set the variable as containing an image.
     *
     * @param img
     *            a image.
     */
    public ImageOrColor(Image img) {
        this.img = img;
        this.isImage = true;
        this.isColor = false;
    }

    /**
     * constructor that get a color. set the variable as containing a color.
     *
     * @param clr
     *            a color
     */
    public ImageOrColor(Color clr) {
        this.clr = clr;
        this.isColor = true;
        this.isImage = false;
    }

    /**
     * @return the image value.
     */
    public Image getImage() {
        return this.img;
    }

    /**
     * @return the color value.
     */
    public Color getColor() {
        return this.clr;
    }

    /**
     * @return whether the variable contains image.
     */
    public boolean isImage() {
        return this.isImage;
    }

    /**
     * @return whether the variable contains color.
     */
    public boolean isColor() {
        return this.isColor;
    }
}
