package levels;

import java.util.List;

import sprites.Block;
import sprites.Sprite;
import sprites.Velocity;

/**
 * interface of LevelInformation. contains query functions for the level
 * features.
 *
 * @author Shachar & Yehoshua.
 *
 */

public interface LevelInformation {
    /**
     * @return the number of balls in the game.
     */

    int numberOfBalls();

    /**
     * insert the velocities of the balls to a list and return it.
     *
     * @return list of velocities.
     */

    List<Velocity> initialBallVelocities();

    /**
     * @return the speed of the paddle.
     */

    int paddleSpeed();

    /**
     * @return the width of the paddle.
     */

    int paddleWidth();

    /**
     * @return the name of the level.
     */

    String levelName();

    /**
     * @return the background of the level.
     */

    Sprite getBackground();

    /**
     * create a list, contains the blocks of this level and return it.
     *
     * @return list of the blocks.
     */

    List<Block> blocks();

    /**
     * @return number of the blocks needed to be remove for ending the game.
     */
    int numberOfBlocksToRemove();
}
