package geometry;
import java.util.List;

/**
 * class of line, contains constructor, query and command functions.
 *
 * @author Shachar & Yehoshua.
 */
public class Line {
    // members
    private Point start;
    private Point end;

    /**
     * constructor of line by getting two points.
     *
     * @param start
     *            first point.
     * @param end
     *            second point.
     */
    public Line(Point start, Point end) {
        // set start and end
        this.start = new Point(start.getX(), start.getY());
        this.end = new Point(end.getX(), end.getY());
    }

    /**
     * constructor of line by getting four coordinates.
     *
     * @param x1
     *            the x coordinate of the first point.
     * @param y1
     *            the y coordinate of the first point.
     * @param x2
     *            the x coordinate of the second point.
     * @param y2
     *            the y coordinate of the second point.
     */
    public Line(double x1, double y1, double x2, double y2) {
        // set start and end
        this.start = new Point(x1, y1);
        this.end = new Point(x2, y2);
    }

    /**
     *
     * @return the length of the line.
     */
    public double length() {
        // the length of the line in the distance between start and end
        return start.distance(end);
    }

    /**
     * calculates the middle point of the line.
     *
     * @return the middle point of the line.
     */
    public Point middle() {
        // calculates the coordinates of the line
        double middleX = (this.start.getX() + this.end.getX()) / 2;
        double middleY = (this.start.getY() + this.end.getY()) / 2;
        // create the point
        Point middlePoint = new Point(middleX, middleY);
        return middlePoint;
    }

    /**
     *
     * @return the start point.
     */
    public Point start() {
        return this.start;
    }

    /**
     *
     * @return the end point.
     */
    public Point end() {
        return this.end;
    }

    /**
     * calculate the orientation of three points.
     *
     * @param a
     *            first point
     * @param b
     *            second point
     * @param c
     *            third point
     * @return the orientation of three points.
     */
    private String orientationType(Point a, Point b, Point c) {
        // calculates the orientation of the three points
        // according to the algorithm in
        // http://www.geeksforgeeks.org/orientation-3-ordered-points/
        double orientSign = (b.getY() - a.getY()) * (c.getX() - b.getX())
                - (c.getY() - b.getY()) * (b.getX() - a.getX());
        // if the orientSign is bigger than 0 the orientation is "clockwise"
        if (orientSign > 0) {
            return "clockwise";
        }
        // if the orientSign is smaller than 0 the orientation is
        // "counterclockwise"
        if (orientSign < 0) {
            return "counterclockwise";
        }
        // if the orientSign is equal to 0 the orientation is "colinear"
        return "colinear";
    }

    /**
     * check whether the lines intersect.
     *
     * @param other
     *            another line
     * @return true if the lines intersect, else false.
     */
    public boolean isIntersecting(Line other) {
        // if the slopes are equal and one of the points is in the both line,
        // there is intersection point.
        double thisSlope = Equation.calculateSlope(this.start, this.end);
        double otherSlope = Equation.calculateSlope(other.start(), other.end());
        if (thisSlope == otherSlope) {
            return this.start.equals(other.start()) || this.start.equals(other.end()) || this.end.equals(other.end())
                    || this.end.equals(other.start());
        }
        // calculate all the orientations
        String thisOrient1 = orientationType(this.start, this.end, other.start());
        String thisOrient2 = orientationType(this.start, this.end, other.end());
        String otherOrient1 = orientationType(other.start(), other.end(), this.start);
        String otherOrient2 = orientationType(other.start(), other.end(), this.end);
        // if each couple of orientations is different, there is intersection
        // point
        if (!thisOrient1.equals(thisOrient2) && !otherOrient1.equals(otherOrient2)) {
            return true;
        }
        return false;
    }

    /**
     * find the intersection point, if it exist.
     *
     * @param other
     *            another line.
     * @return the intersection point if the lines intersect, and null
     *         otherwise.
     */
    public Point intersectionWith(Line other) {
        double coordinateX;
        double coordinateY;
        // if the lines are not intersecting the intersection point is not exist
        if (!this.isIntersecting(other)) {
            return null;
        }
        // calculates the 2 equations
        Equation thisEquation = new Equation(this);
        Equation otherEquation = new Equation(other);
        // if one of the lines is vertical the coordinateX is the x coordinate
        // of one of the points in the vertical line
        if (thisEquation.isConstEquation()) {
            coordinateX = thisEquation.getXConst();
            coordinateY = otherEquation.calculateYValue(coordinateX);
        } else {
            if (otherEquation.isConstEquation()) {
                // calculates the coordinateY of the coordinateX according to
                // one
                // equation
                coordinateX = otherEquation.getXConst();
            } else {
                // if neither one of the lines is vertical the coordinateX is
                // depend on the y intercepts and slopes
                coordinateX = (otherEquation.getYIntercept() - thisEquation.getYIntercept())
                        / (thisEquation.getSlope() - otherEquation.getSlope());
            }
            // calculates the coordinateY of the coordinateX according to
            // one
            // equation
            coordinateY = thisEquation.calculateYValue(coordinateX);
        }
        // create the intersection point
        Point intersectPoint = new Point(coordinateX, coordinateY);
        return intersectPoint;
    }

    /**
     * find the closest point that intersect the start of line.
     *
     * @param rect
     *            a rectangle.
     * @return null if the line doesn't intersect with the rectangle else return
     *         the closest intersection point to the start of the line.
     */
    public Point closestIntersectionToStartOfLine(Rectangle rect) {
        //get list of the intersection points between this line and a rectangle
        List intersectionPoints = rect.intersectionPoints(this);
        if (intersectionPoints.isEmpty()) {
            return null;
        }
        //find the closest point by distance
        Point closestToStart = (Point) intersectionPoints.get(0);
        for (int i = 1; i < intersectionPoints.size(); i++) {
            Point nextClose = (Point) intersectionPoints.get(i);
            if (this.start.distance(nextClose) < this.start.distance(closestToStart)) {
                closestToStart = nextClose;
            }
        }
        return closestToStart;
    }

    /**
     * check whether two lines are equal.
     *
     * @param other
     *            another line.
     * @return true if the lines are equal, else false.
     */
    public boolean equals(Line other) {
        // check whether the lines are equal
        if ((this.start.equals(start) && this.end.equals(end)) || (this.end.equals(start) && this.start.equals(end))) {
            return true;
        }
        return false;
    }
}