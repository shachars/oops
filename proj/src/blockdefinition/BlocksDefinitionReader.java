package blockdefinition;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class of BlocksDefinitionReader. read the definition file and create a
 * blocks factory according to it.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class BlocksDefinitionReader {
    private static Map<String, Integer> spaceMap;
    private static Map<String, String> defaultsMap;
    private static List<Map<String, String>> blockDefList;

    /**
     * return a blocks factory.
     *
     * @param reader
     *            a Reader object that can read from a definition file.
     * @return a blocks factory.
     */
    public static BlocksFromSymbolsFactory fromReader(java.io.Reader reader) {
        return getFactory(reader);
    }

    /**
     * create a blocks factory from the file.
     *
     * @param reader
     *            a Reader object that can read from a definition file.
     * @return a blocks factory.
     */
    private static BlocksFromSymbolsFactory getFactory(Reader reader) {
        BufferedReader bufRead = new BufferedReader(reader);
        spaceMap = new HashMap<>();
        defaultsMap = new HashMap<>();
        blockDefList = new ArrayList<>();
        Map<String, BlockCreator> blockCreators = new HashMap<String, BlockCreator>();
        String line;
        try {
            while ((line = bufRead.readLine()) != null) {
                if (line.startsWith("default")) {
                    interceptDefault(line);
                }
                if (line.startsWith("bdef")) {
                    blockDefList.add(interceptBlock(line));
                }
                if (line.startsWith("sdef")) {
                    interceptSpace(line);
                }
            }
            for (Map<String, String> map : blockDefList) {
                addBlockCreator(map, blockCreators);
            }
        } catch (IOException e) {
            System.out.println("problem in reading from BlockDefinition file");
        }
        return new BlocksFromSymbolsFactory(spaceMap, blockCreators);
    }

    /**
     * add the line to the default map.
     *
     * @param line
     *            a line of the defaults of all the blocks.
     */
    private static void interceptDefault(String line) {
        String[] defParts = line.split("\\s");
        for (int i = 1; i < defParts.length; i++) {
            defaultsMap.put(defParts[i].split(":")[0], (defParts[i].split(":")[1]));
        }
    }

    /**
     * add the line to the spaces map.
     *
     * @param line
     *            a line of one space symbol.
     */
    private static void interceptSpace(String line) {
        String[] defParts = line.split("\\s");
        String splitedSymbol = defParts[1].split(":")[1];
        String splitedWidth = defParts[2].split(":")[1];
        spaceMap.put(splitedSymbol, Integer.parseInt(splitedWidth));
    }

    /**
     * make a map that map the block's characters.
     *
     * @param line
     *            a line of one block symbol.
     * @return a map that map the block's characters.
     */
    private static Map<String, String> interceptBlock(String line) {
        Map<String, String> lineMap = new HashMap<String, String>();
        String[] defParts = line.split("\\s");
        for (int i = 1; i < defParts.length; i++) {
            lineMap.put(defParts[i].split(":")[0], defParts[i].split(":")[1]);
        }
        return lineMap;
    }

    /**
     * add one blockCreator to the blockCreators map.
     *
     * @param map
     *            add one
     * @param blockCreators
     *            a map that map symbols to a maps of their blocks.
     */
    private static void addBlockCreator(Map<String, String> map, Map<String, BlockCreator> blockCreators) {
        String symbol = map.get("symbol");
        map.putAll(defaultsMap);
        Map<String, String> blockCharacts = map;
        blockCreators.put(symbol, new ImageOrColorBlockCreator(blockCharacts));
    }
}
