package animation;

/**
 * interface of Task<T> - generic task.
 *
 * @author Shachar & Yehoshua.
 *
 * @param <T>
 *            task to run
 */
public interface Task<T> {
    /**
     * the function run the task.
     *
     * @return null
     */
    T run();
}
