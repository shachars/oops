package blockdefinition;

import java.util.Map;

import sprites.Block;

/**
 * a factory of blocks and spaces according to symblos.
 * it's kinda in the name...
 *
 * @author Shachar & Yehoshua.
 *
 */
public class BlocksFromSymbolsFactory {
    private Map<String, Integer> spaceMap;
    private Map<String, BlockCreator> blockCreators;

    /**
     * constructor of BlocksFromSymbolsFactory.
     *
     * @param spaceMap
     *            a map mapping a symbol to the his space width
     * @param blockCreators
     *            a map mapping a symbol to the his block
     */
    public BlocksFromSymbolsFactory(Map<String, Integer> spaceMap, Map<String, BlockCreator> blockCreators) {
        this.spaceMap = spaceMap;
        this.blockCreators = blockCreators;
    }

    /**
     * returns true if 's' is a valid space symbol.
     *
     * @param s
     *            a String object.
     * @return true if 's' is a valid space symbol.
     */
    public boolean isSpaceSymbol(String s) {
        return this.spaceMap.containsKey(s);
    }

    /**
     * returns true if 's' is a valid block symbol.
     *
     * @param s
     *            a String object.
     * @return true if 's' is a valid block symbol.
     */
    public boolean isBlockSymbol(String s) {
        if (this.blockCreators.containsKey(s)) {
            return true;
        }
        return false;
    }

    /**
     * Return a block according to the definitions associated with symbol s. The
     * block will be located at position (xpos, ypos).
     *
     * @param s
     *            symbol.
     * @param xpos
     *            the x coordinate of the block.
     * @param ypos
     *            the y coordinate of the block.
     * @return a block according to the definitions associated with symbol s.
     */
    public Block getBlock(String s, int xpos, int ypos) {
        BlockCreator blCreator = this.blockCreators.get(s);
        return blCreator.create(xpos, ypos);
    }

    /**
     * Returns the width in pixels associated with the given spacer-symbol.
     *
     * @param s
     *            a String object.
     * @return the width in pixels associated with the given spacer-symbol.
     */
    public int getSpaceWidth(String s) {
        return this.spaceMap.get(s);
    }
}
