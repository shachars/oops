package animation;

/**
 * class of Selection, defines selections.
 *
 * @author Shachar & Yehoshua.
 *
 * @param <T>
 *            generic value for Selection
 */
public class Selection<T> {
    // members
    private T returnVal;
    private String key;
    private String message;

    /**
     * constructor of Selection.
     *
     * @param key
     *            key to press
     * @param message
     *            message to show
     * @param returnVal
     *            value to return
     */
    public Selection(String key, String message, T returnVal) {
        this.key = key;
        this.message = message;
        this.returnVal = returnVal;
    }

    /**
     * @return key of selection
     */
    public String getKey() {
        return this.key;
    }

    /**
     * @return message of selection
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @return return value of selection
     */
    public T getReturnVal() {
        return this.returnVal;
    }
}
