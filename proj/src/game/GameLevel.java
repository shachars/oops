package game;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import animation.Animation;
import animation.AnimationRunner;
import animation.CountdownAnimation;
import animation.KeyPressStoppableAnimation;
import animation.PauseScreen;
import biuoop.DrawSurface;
import biuoop.KeyboardSensor;
import blockdefinition.ImageOrColor;
import geometry.Point;
import geometry.Rectangle;
import levels.LevelInformation;
import listeners.BallRemover;
import listeners.BlockRemover;
import listeners.ScoreTrackingListener;
import sprites.Ball;
import sprites.Block;
import sprites.Collidable;
import sprites.Paddle;
import sprites.Sprite;
import sprites.SpriteCollection;

/**
 * class of GameLevel. in charge of initialize the game and play one turn.
 * implements Animation.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class GameLevel implements Animation {
    // members
    private SpriteCollection sprites;
    private GameEnvironment environment;
    private Counter blockCounter;
    private Counter ballCounter;
    private Counter scoreCounter;
    private Counter livesCounter;
    private AnimationRunner runner;
    private KeyboardSensor keyboard;
    private boolean running;
    private LevelInformation levInfo;

    /**
     * constructor of GameLevel.
     *
     * @param levInfo
     *            information of the level to initialize.
     * @param scoreCounter
     *            Counter object that counts the lives.
     * @param livesCounter
     *            Counter object that counts the lives.
     * @param runner
     *            Animation runner object, runs the animation
     * @param keyboard
     *            KeyboardSensor object.
     */
    public GameLevel(LevelInformation levInfo, Counter scoreCounter, Counter livesCounter, AnimationRunner runner,
            KeyboardSensor keyboard) {
        this.sprites = new SpriteCollection();
        this.environment = new GameEnvironment();
        this.blockCounter = new Counter();
        this.ballCounter = new Counter();
        this.scoreCounter = scoreCounter;
        this.livesCounter = livesCounter;
        this.runner = runner;
        this.running = true;
        this.keyboard = keyboard;
        this.levInfo = levInfo;
    }

    /**
     * add a collidable to the gameEnviroment.
     *
     * @param c
     *            a collidable object.
     */
    public void addCollidable(Collidable c) {
        this.environment.addCollidable(c);
    }

    /**
     * add a sprite to the SpriteCollection.
     *
     * @param s
     *            a sprite object.
     */
    public void addSprite(Sprite s) {
        this.sprites.addSprite(s);
    }

    /**
     * Initialize a new level: add boundaries, blocks,death region and the
     * indicators.
     */
    public void initialize() {
        this.addSprite(this.levInfo.getBackground());
        addBoundaries();
        addBlocks();
        addDeathRegion();
        addIndicator();
    }

    /**
     * add lives, score and name indicators to the game.
     */
    private void addIndicator() {
        ScoreIndicator scoreIndicator = new ScoreIndicator(this.scoreCounter);
        scoreIndicator.addToGame(this);
        LivesIndicator livesIndicator = new LivesIndicator(this.livesCounter);
        livesIndicator.addToGame(this);
        NameIndicator nameIndicator = new NameIndicator(this.levInfo.levelName());
        nameIndicator.addToGame(this);
    }

    /**
     * Play one turn of the game. adds balls and paddle to the game.
     */
    public void playOneTurn() {
        addBalls();
        Paddle paddle = addPaddle();
        // run the countdown animation.
        this.runner.run(new CountdownAnimation(2000, 3, this.sprites));
        this.running = true;
        this.runner.run(this);
        paddle.removeFromGame(this);
        // loose life when all the balls fall from screen
        if (this.ballCounter.isZero()) {
            this.livesCounter.decrease(1);
        }
    }

    /**
     * add blocks in the boundaries of the GUI.
     */
    private void addBoundaries() {
        int boundSize = 25;
        // creating 3 rectangles
        int height = 600;
        int width = 800;
        Rectangle rect1 = new Rectangle(new Point(0, 20), width, boundSize);
        Rectangle rect2 = new Rectangle(new Point(0, 0), boundSize, height);
        Rectangle rect3 = new Rectangle(new Point(width - boundSize, 0), boundSize, height);
        Rectangle[] rectArray = {rect1, rect2, rect3};
        Map<Integer, ImageOrColor> mapImOrClr = new HashMap<Integer, ImageOrColor>();
        ImageOrColor imOrClr = new ImageOrColor(Color.CYAN);
        mapImOrClr.put(0, imOrClr);
        Color stroke = Color.BLACK;
        // draw the boundaries
        for (int i = 0; i < rectArray.length; i++) {
            Block boundary = new Block(rectArray[i], mapImOrClr, 0, stroke);
            boundary.addToGame(this);
        }
    }

    /**
     * add the blocks according to the level information.
     */
    public void addBlocks() {
        this.blockCounter.increase(this.levInfo.numberOfBlocksToRemove());
        BlockRemover br = new BlockRemover(this, this.blockCounter);
        ScoreTrackingListener sl = new ScoreTrackingListener(this.scoreCounter);
        for (Block block : this.levInfo.blocks()) {
            block.addHitListener(br);
            block.addHitListener(sl);
            block.addToGame(this);
        }
    }

    /**
     * add balls to the game according to the level information.
     */
    public void addBalls() {
        for (int i = 0; i < this.levInfo.numberOfBalls(); i++) {
            Ball ball = new Ball(400, 560, 5, Color.WHITE, this.environment);
            ball.setVelocity(this.levInfo.initialBallVelocities().get(i));
            ball.addToGame(this);
            ballCounter.increase(1);
        }
    }

    /**
     * add the paddle to the game according to the level information.
     *
     * @return this paddle.
     */
    public Paddle addPaddle() {
        int boundsSize = 25;
        // creating a paddle according to gui and the boundaries
        Paddle paddle = new Paddle(this.keyboard, boundsSize, this.runner.getGui().getDrawSurface().getWidth(),
                this.runner.getGui().getDrawSurface().getHeight(), this.levInfo.paddleWidth(),
                this.levInfo.paddleSpeed());
        paddle.addToGame(this);
        return paddle;
    }

    /**
     * add the death region to the game.
     */
    private void addDeathRegion() {
        BallRemover blr = new BallRemover(this, this.ballCounter);
        Rectangle rect = new Rectangle(new Point(0, 600), 800, 0);
        Map<Integer, ImageOrColor> mapImOrClr = new HashMap<Integer, ImageOrColor>();
        ImageOrColor imOrClr = new ImageOrColor(Color.CYAN);
        mapImOrClr.put(0, imOrClr);
        Color stroke = Color.BLACK;
        Block death = new Block(rect, mapImOrClr, 0, stroke);
        death.addHitListener(blr);
        death.addToGame(this);
    }

    /**
     * remove collidables from list.
     *
     * @param c
     *            collidaable object to remove.
     */
    public void removeCollidable(Collidable c) {
        this.environment.removeCollidable(c);
    }

    /**
     * remove sprites from list.
     *
     * @param s
     *            sprite object to remove.
     */
    public void removeSprite(Sprite s) {
        this.sprites.removeSprite(s);
    }

    /**
     * show one frame of the game.
     *
     * @param d
     *            DrawSurface object
     * @param dt
     *            seconds passed since last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        // draw all sprites and notify them time passed.
        this.sprites.drawAllOn(d);
        this.sprites.notifyAllTimePassed(dt);
        // press 'p' for pause
        if (this.keyboard.isPressed("p")) {
            this.runner.run(new KeyPressStoppableAnimation(this.keyboard, KeyboardSensor.SPACE_KEY, new PauseScreen()));
        }
        // if the player removed all the blocks
        if (this.blockCounter.getValue() == 0) {
            this.scoreCounter.increase(100);
            this.running = false;
        }
        // if the player lost life.
        if (this.ballCounter.getValue() == 0) {
            this.running = false;
        }

    }

    /**
     * @return number of blocks
     */
    public Counter getBlocksCounter() {
        return this.blockCounter;
    }

    /**
     * @return if the animation loop should stop
     */
    public boolean shouldStop() {
        return !this.running;
    }
}
