package animation;

import java.util.ArrayList;
import java.util.List;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;

/**
 * class of MenuAnimation, implements Menu. shows the menu screen.
 *
 * @author Shachar & Yehoshua.
 *
 * @param <T>
 *            generic parameter to run
 */
public class MenuAnimation<T> implements Menu<T> {
    // members..
    private KeyboardSensor keyboard;
    private boolean stop;
    private List<Selection<T>> selectionList;
    private List<SubMenuSelection<T>> subMenuList;
    private AnimationRunner runner;

    /**
     * constructor of MenuAnimation.
     *
     * @param k
     *            KeyboardSensor object
     * @param runner
     *            AnimationRunner object
     */
    public MenuAnimation(KeyboardSensor k, AnimationRunner runner) {
        this.runner = runner;
        this.keyboard = k;
        this.stop = false;
        this.selectionList = new ArrayList<Selection<T>>();
        this.subMenuList = new ArrayList<SubMenuSelection<T>>();
    }

    /**
     * show one frame of the specific animation.
     *
     * @param d
     *            a DrawSurface object
     * @param dt
     *            seconds passed since last call.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        int index = 0;
        for (SubMenuSelection<T> subMenuSelection : subMenuList) {
            d.drawText(10, d.getHeight() / 4 + index * 40,
                    "(" + subMenuSelection.getKey() + ")" + " " + subMenuSelection.getMessage(), 32);
            index++;
        }
        for (Selection<T> selection : selectionList) {
            d.drawText(10, d.getHeight() / 4 + index * 40,
                    "(" + selection.getKey() + ")" + " " + selection.getMessage(), 32);
            index++;
        }
        for (SubMenuSelection<T> subMenuSelection : subMenuList) {
            if (this.keyboard.isPressed(subMenuSelection.getKey())) {
                this.runner.run(subMenuSelection.getSubMenu());
                Task<Void> task = (Task<Void>) subMenuSelection.getSubMenu().getStatus();
                task.run();
                subMenuSelection.getSubMenu().reset();
            }
        }
        for (Selection<T> selection : selectionList) {
            if (this.keyboard.isPressed(selection.getKey())) {
                this.stop = true;
            }
        }
    }

    /**
     * @return running situation.
     */
    public boolean shouldStop() {
        return this.stop;
    }

    /**
     * add selection to selection list.
     *
     * @param key
     *            key to press
     * @param message
     *            message to show
     * @param returnVal
     *            value to return
     */
    public void addSelection(String key, String message, T returnVal) {
        this.selectionList.add(new Selection<T>(key, message, returnVal));
    }

    /**
     * @return the status according to the key pressed
     */
    public T getStatus() {
        for (Selection<T> selection : selectionList) {
            if (this.keyboard.isPressed(selection.getKey())) {
                return selection.getReturnVal();
            }
        }
        return null;
    }

    /**
     * set the stopper as false.
     */
    public void reset() {
        this.stop = false;
    }

    /**
     * add sub menu.
     *
     * @param key
     *            key to press
     * @param message
     *            message to show
     * @param subMenu
     *            a sub menu
     */
    public void addSubMenu(String key, String message, Menu<T> subMenu) {
        this.subMenuList.add(new SubMenuSelection<T>(key, message, subMenu));
    }
}
