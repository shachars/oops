package animation;

import biuoop.DrawSurface;
import biuoop.KeyboardSensor;

/**
 * Class of KeyPressStoppableAnimation. implements Animation. run an inner
 * animation till the endkey is pressed.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class KeyPressStoppableAnimation implements Animation {
    private KeyboardSensor keySensor;
    private String endkey;
    private Animation animation;
    private boolean stop;
    private boolean isAlreadyPressed;

    /**
     * constructor of KeyPressStoppableAnimation.
     *
     * @param sensor
     *            a KeyboardSensor object.
     * @param key
     *            the end key
     * @param animation
     *            an inner animation
     */
    public KeyPressStoppableAnimation(KeyboardSensor sensor, String key, Animation animation) {
        this.keySensor = sensor;
        this.endkey = key;
        this.animation = animation;
        this.stop = false;
        this.isAlreadyPressed = true;
    }

    /**
     * run doOneFrame of the decorated animation till the end key isPressed.
     *
     * @param d
     *            a DrawSurface object
     * @param dt
     *            the time passed since last run.
     */
    public void doOneFrame(DrawSurface d, double dt) {
        this.animation.doOneFrame(d, dt);
        if (!this.keySensor.isPressed(this.endkey)) {
            this.isAlreadyPressed = false;
        } else {
            if (!isAlreadyPressed) {
                this.stop = true;
            }
        }
    }

    /**
     * @return combined running situation of the both decorator and inner
     *         animation
     */
    public boolean shouldStop() {
        return this.stop;
    }
}
