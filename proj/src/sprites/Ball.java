package sprites;

import java.awt.Color;

import biuoop.DrawSurface;
import game.GameEnvironment;
import game.GameLevel;
import geometry.Line;
import geometry.Point;
import geometry.Rectangle;

/**
 * class of ball. contains constructor and query and command functions. can draw
 * the ball and move it to new location.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Ball implements Sprite {
    // members
    private int radius;
    private Point center;
    private java.awt.Color color;
    private Velocity velocity;
    private GameEnvironment gameEnvironment;

    /**
     * constructor of ball according to center point.
     *
     * @param center
     *            the point of the center of the ball.
     * @param r
     *            the radius of the ball.
     * @param color
     *            the color of the ball.
     * @param gameEnvironment
     *            the environment of the game
     */
    public Ball(Point center, int r, java.awt.Color color, GameEnvironment gameEnvironment) {
        // set radius, center, color, border and game environment
        this.radius = r;
        this.center = center;
        this.color = color;
        this.gameEnvironment = gameEnvironment;
        // set default velocity
        this.velocity = new Velocity(0, 0);

    }

    /**
     * constructor of ball according to center coordinates.
     *
     * @param x
     *            the x coordinate of the center point.
     * @param y
     *            the y coordinate of the center point.
     * @param r
     *            the radius of the ball.
     * @param color
     *            the color of the ball.
     * @param gameEnvironment
     *            the environment of the game
     */
    public Ball(int x, int y, int r, java.awt.Color color, GameEnvironment gameEnvironment) {
        // set radius, center, color, border and game environment
        this.radius = r;
        this.center = new Point(x, y);
        this.color = color;
        this.gameEnvironment = gameEnvironment;
        // set default velocity
        this.velocity = new Velocity(0, 0);
    }

    /**
     * @return the x coordinate of the center point.
     */
    public int getX() {
        return (int) this.center.getX();
    }

    /**
     * @return the y coordinate of the center point.
     */
    public int getY() {
        return (int) this.center.getY();
    }

    /**
     * @return the radius of the ball.
     */
    public int getRadius() {
        return this.radius;
    }

    /**
     * @return the color of the ball.
     */
    public java.awt.Color getColor() {
        return this.color;
    }

    /**
     * draw the ball.
     *
     * @param surface
     *            a DrawSurface object.
     */
    public void drawOn(DrawSurface surface) {
        // draw a ball
        surface.setColor(Color.BLACK);
        surface.drawCircle((int) this.center.getX(), (int) this.center.getY(), this.radius);
        surface.setColor(this.color);
        surface.fillCircle((int) this.center.getX(), (int) this.center.getY(), this.radius);
    }

    /**
     * set the ball's velocity by getting a Velocity object.
     *
     * @param v
     *            the velocity of the ball.
     */
    public void setVelocity(Velocity v) {
        this.velocity = new Velocity(v.getDX(), v.getDY());
    }

    /**
     * set the ball's velocity by getting a dx and dy.
     *
     * @param dx
     *            the dx of the velocity
     * @param dy
     *            the dx of the velocity
     */
    public void setVelocity(double dx, double dy) {
        this.velocity = new Velocity(dx, dy);
    }

    /**
     * @return the velocity of the ball.
     */
    public Velocity getVelocity() {
        return this.velocity;
    }

    /**
     * create a line from the center of the ball to the next center after moving
     * it one time.
     *
     * @param dt
     *            seconds passed since last call.
     * @return the trajectory line
     */
    public Line trajectoryCompute(double dt) {
        // create the ball's trajectory line
        return new Line(this.center, this.velocity.applyToPoint(this.center, dt));
    }

    /**
     * determines the velocity of the ball according to its location and border
     * and then move the ball's center to next location.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void moveOneStep(double dt) {
        CollisionInfo collisionInfo;
        // get the trajectory line
        Line l = trajectoryCompute(dt);
        // get the closest collision
        collisionInfo = gameEnvironment.getClosestCollision(l);
        if (collisionInfo == null) {
            // collision didn't happen so the ball move to the next position
            this.center = this.getVelocity().applyToPoint(this.center, dt);
        } else {
            // getting the collision rectangle and dy,dx
            Rectangle collisionRect = collisionInfo.collisionObject().getCollisionRectangle();
            Point collisionPoint = collisionInfo.collisionPoint();
            double dx = collisionPoint.getX();
            double dy = collisionPoint.getY();
            // getting the lineText contains string of the lines that the
            // collision point in them
            String lineText = collisionRect.whichLine(collisionPoint);
            // moving the ball almost to the collision point according to the
            // side of the block that the ball hit
            if (lineText.contains("UP")) {
                dy -= 1;
            }
            if (lineText.contains("DOWN")) {
                dy += 1;
            }
            if (lineText.contains("RIGHT")) {
                dx += 1;
            }
            if (lineText.contains("LEFT")) {
                dx -= 1;
            }
            // changing the center to the almost collision point
            this.center = new Point(dx, dy);
            // changing the velocity according to the side of the block that the
            // ball hit
            this.velocity = collisionInfo.collisionObject().hit(this, collisionInfo.collisionPoint(), this.velocity);
        }
    }

    /**
     * notify the sprite that time has passed by calling moveOneStep.
     *
     * @param dt
     *            seconds passed since last call.
     */
    public void timePassed(double dt) {
        this.moveOneStep(dt);
    }

    /**
     * add the paddle to the game.
     *
     * @param g
     *            a Game object
     */
    public void addToGame(GameLevel g) {
        g.addSprite(this);
    }

    /**
     * remove the paddle from the game.
     *
     * @param g
     *            a gameLevel object.
     */
    public void removeFromGame(GameLevel g) {
        g.removeSprite(this);
    }
}