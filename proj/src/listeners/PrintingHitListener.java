package listeners;

import sprites.Ball;
import sprites.Block;

/**
 * Class of PrintingHitListener. implements HitListener. prints a message about
 * the hitting whenever block got hit.
 *
 * @author Shachar & Yehoshua
 *
 */
public class PrintingHitListener implements HitListener {
    /**
     * This method is called whenever the beingHit object is hit. then the
     * listener prints a message about the hitting.
     *
     * @param beingHit
     *            the notifier.
     * @param hitter
     *            the ball that hit the notifier.
     */
    public void hitEvent(Block beingHit, Ball hitter) {
        System.out.println("A Block with " + beingHit.getHitPoints() + " points was hit.");
    }
}