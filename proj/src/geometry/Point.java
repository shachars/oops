package geometry;
/**
 * class of point, contains constructor, query and command functions.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Point {
    // members
    private double x;
    private double y;

    /**
     * constructor of point.
     *
     * @param x
     *            coordinate 1
     * @param y
     *            coordinate 2
     *
     */
    public Point(double x, double y) {
        //set members
        this.x = x;
        this.y = y;
    }

    /**
     * calculate the distance between two points.
     *
     * @param other
     *            another point
     * @return the distance between the points.
     */
    public double distance(Point other) {
        // calculate the distance according to pythagoras theorem
        double powerY = Math.pow(this.y - other.y, 2);
        double powerX = Math.pow(this.x - other.x, 2);
        double distance = Math.sqrt(powerY + powerX);
        return distance;
    }

    /**
     * check whether the points are equal.
     *
     * @param other
     *            another point
     * @return true if the points equal, else return false
     */
    public boolean equals(Point other) {
        // if the y coordinates are different
        if (this.y != other.y) {
            return false;
        }
        // if the x coordinates are different
        if (this.x != other.x) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return the x value of the point
     */
    public double getX() {
        return this.x;
    }

    /**
     *
     * @return the y value of the point
     */
    public double getY() {
        return this.y;
    }
}
