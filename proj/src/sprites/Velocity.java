package sprites;

import geometry.Point;

/**
 * class of velocity, contains constructor, query and command functions.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class Velocity {
    // members
    private double dx;
    private double dy;

    /**
     * constructor of velocity.
     *
     * @param dx
     *            the change in x axes
     * @param dy
     *            the change in x axes
     */
    public Velocity(double dx, double dy) {
        // set members
        this.dx = dx;
        this.dy = dy;
    }

    /**
     * calculate the dx and dy by getting the speed and angle.
     *
     * @param angle
     *            the direction of the ball
     * @param speed
     *            the spped of the ball
     * @return new velocity
     */
    public static Velocity fromAngleAndSpeed(double angle, double speed) {
        // calculate the dx and dy by getting the speed and angle
        double dx = speed * Math.sin(Math.toRadians(angle));
        double dy = speed * -Math.cos(Math.toRadians(angle));
        return new Velocity(dx, dy);
    }

    /**
     * Take a point with position (x,y) and return a new point with position
     * (x+dx, y+dy).
     *
     * @param p
     *            point
     * @param dt
     *            seconds passed since last call.
     * @return point after change
     */
    public Point applyToPoint(Point p, double dt) {
        // Take a point with position (x,y) and return a new point with position
        // (x+dx, y+dy).
        Point newPoint = new Point(this.dx * dt + p.getX(), this.dy * dt + p.getY());
        return newPoint;
    }

    /**
     * @return dx
     */
    public double getDX() {
        return this.dx;
    }

    /**
     * @return dy
     */
    public double getDY() {
        return this.dy;
    }

    /**
     * calculate the speed from dy, dx.
     *
     * @return the speed of the ball.
     */
    public double calculateSpeed() {
        // the dy, dx and speed create a right triangle
        // the calculation is according to Pythagoras theorem
        return Math.sqrt(Math.pow(this.dx, 2) + Math.pow(this.dy, 2));
    }

    /**
     * calculate the angle from dy, dx.
     *
     * @return the angle of the ball.
     */
    public double calculateAngle() {
        double angleRadian = Math.asin(this.dx / this.calculateSpeed());
        return Math.toDegrees(angleRadian);
    }
}