package sprites;
import geometry.Point;

/**
 * class of collision info, contains constructor and query functions.
 *
 * @author Shachar & Yehoshua.
 *
 */
public class CollisionInfo {
    // members
    private Point collisionPoint;
    private Collidable collisionObject;

    /**
     * constructor of collisionInfo.
     *
     * @param closestCollisionPoint
     *            on the trajectory
     * @param closestCollide
     *            on the trajectory
     */
    public CollisionInfo(Point closestCollisionPoint, Collidable closestCollide) {
        //set the members
        this.collisionPoint = closestCollisionPoint;
        this.collisionObject = closestCollide;
    }

    /**
     * @return the point at which the collision occurs.
     */
    public Point collisionPoint() {
        return this.collisionPoint;
    }

    /**
     * @return the collidable object involved in the collision.
     */
    public Collidable collisionObject() {
        return this.collisionObject;
    }
}